<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 10);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 144px center no-repeat;
  }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
      <div class="col-xs-8 top45 text-center titulo_contatos">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_balanca.png" alt="" />
        <h3 class="top15">MARQUE UMA HORA CONOSCO</h3>
        <i class="fa fa-angle-down" aria-hidden="true"></i>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
  </div>
 </div>




 <div class="container">
   <div class="row">


     <!-- ======================================================================= -->
     <!--CONTATOS   -->
     <!-- ======================================================================= -->
     <div class="col-xs-12 top10">
         <?php require_once('./includes/contatos.php'); ?>
     </div>
     <!-- ======================================================================= -->
     <!--CONTATOS   -->
     <!-- ======================================================================= -->

     <div class="col-xs-12">
       <!-- ======================================================================= -->
       <!-- Breadcrumbs    -->
       <!-- ======================================================================= -->
         <div class="breadcrumb top20">
           <a class="face" href="https://www.facebook.com" target="_blank" data-toggle="tooltip" title="facebook">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_face.png" alt="" />
					</a>
           <a class="active1">VOCÊ ESTÁ EM:</a>
           <a href="<?php echo Util:: caminho_projeto() ?>/" data-toggle="tooltip" title="HOME">HOME</i></a>
           <a class="active">AGENDAR UMA VISTA</a>
           </div>
           <!-- ======================================================================= -->
           <!-- Breadcrumbs    -->
           <!-- ======================================================================= -->
     </div>


     <div class="col-xs-8 padding0 agende_formulario">

             <div class="top15">




                 <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
                   <div class="fundo_formulario">
                     <!-- formulario orcamento -->
                     <div class="top20">
                       <div class="col-xs-6">
                         <div class="form-group input100 has-feedback">
                           <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                            <span class="fa fa-home form-control-feedback top20"></span>
                         </div>
                       </div>

                       <div class="col-xs-6">
                         <div class="form-group  input100 has-feedback">
                           <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                            <span class="fa fa-envelope form-control-feedback top20"></span>
                         </div>
                       </div>
                     </div>

                     <div class="clearfix"></div>


                     <div class="top20">
                       <div class="col-xs-6">
                         <div class="form-group  input100 has-feedback">
                           <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                            <span class="fa fa-phone form-control-feedback top20"></span>
                         </div>
                       </div>

                       <div class="col-xs-6">
                         <div class="form-group  input100 has-feedback">
                           <input type="text" name="endereco" class="form-control fundo-form1 input-lg input100" placeholder="ENDEREÇO">
                            <span class="fa fa-globe form-control-feedback top15"></span>
                         </div>
                       </div>
                     </div>

                     <div class="clearfix"></div>

                     <div class="top20">


                       <div class="col-xs-6">
                         <div class="form-group  input100 has-feedback">
                           <input type="text" id="data" name="data" class="form-control fundo-form1 input-lg input100" placeholder="dd/mm/yyyy">
                            <span class="fa fa-calendar form-control-feedback top10"></span>
                         </div>
                       </div>


                       <div class="col-xs-6">
                         <div class="form-group  input100 has-feedback">
                           <input type="text" id="hora" name="hora_visita" class="form-control fundo-form1 input-lg input100" placeholder="00:00">
                            <span class="fa fa-clock-o form-control-feedback top10"></span>
                         </div>
                       </div>
                     </div>

                     <div class="clearfix"></div>


                     <div class="top20">
                       <div class="col-xs-6">
                         <div class="form-group  input100 has-feedback">
                           <input type="text" name="conheceu" class="form-control fundo-form1 input-lg input100" placeholder="COMO NOS CONHECEU">
                            <span class="fa fa-folder form-control-feedback top5"></span>
                         </div>
                       </div>
                     </div>

                     <div class="clearfix"></div>

                     <div class="top15">
                       <div class="col-xs-12">
                        <div class="form-group input100 has-feedback">
                         <textarea name="mensagem" cols="30" rows="9" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                         <span class="fa fa-pencil form-control-feedback top20"></span>

                       </div>
                     </div>
                   </div>

                   <!-- formulario orcamento -->
                   <div class="col-xs-12 text-right">
                     <div class="top20 bottom25">
                       <button type="submit" class="btn btn_formulario" name="btn_contato">
                         ENVIAR
                       </button>
                     </div>
                   </div>

                 </div>


                 <!--  ==============================================================  -->
                 <!-- FORMULARIO-->
                 <!--  ==============================================================  -->
               </form>

           </div>
           </div>

   </div>
 </div>



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>

<!-- ======================================================================= -->
<!-- JS  e CSS   -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- JS  e CSS   -->
<!-- ======================================================================= -->


<?php
    //  VERIFICO SE E PARA ENVIAR O EMAIL
    if(isset($_POST[nome]))
    {
       $texto_mensagem = "
                          Nome: ".($_POST[nome])." <br />
                          Email: ".($_POST[email])." <br />
                          Telefone: ".($_POST[telefone])." <br />
                          Endereço: ".($_POST[endereco])." <br />
                          Data da Visita: ".($_POST[data])." <br />
                          Hora da visita: ".($_POST[hora_visita])." <br />
                          Como nos Conheçeu: ".($_POST[conheceu])." <br />


                          Mensagem: <br />
                          ".(nl2br($_POST[mensagem]))."
                          ";


        Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
        Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
        Util::alert_bootstrap("Obrigado por entrar em contato.");
        unset($_POST);
    }


    ?>



<script>
  $(document).ready(function() {
    $('.FormContatos').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      data11: {
       validators: {
         notEmpty: {
           message: 'Informe uma data.'

         }
       }
     },
     hora_visita1: {
       validators: {
         notEmpty: {
           message: 'Informe um horário de visita.'
         }
       }
    },
      endereco: {
       validators: {
         notEmpty: {
            message: 'Informe seu endereço ?'
         }
       }
     },
      email: {
        validators: {
          notEmpty: {
            message: 'por favor insira seu e-mail'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
         validators: {
           notEmpty: {
             message: 'por favor informe seu numero'
           },
           phone: {
               country: 'BR',
               message: 'Informe um telefone válido.'
           }
         },
       },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });


  });
</script>


<!-- ======================================================================= -->
<!-- MOMENTS  -->
<!-- ======================================================================= -->
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript">

$('#hora').datetimepicker({
  format: 'LT'
});

</script>
