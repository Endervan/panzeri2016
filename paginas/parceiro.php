<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_equipes", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/parceiros");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 9) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 144px center no-repeat;
}
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
      <div class="col-xs-8 top55 bottom30 text-center titulo_contatos">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_balanca.png" alt="" />
        <h3 class="top15">PARCEIROS DE NEGÓCIOS</h3>
        <i class="fa fa-angle-down" aria-hidden="true"></i>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
    </div>
  </div>




  <div class="container">
    <div class="row">



      <div class="col-xs-12 bottom10">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
        <div class="breadcrumb">
          <a class="face" href="https://www.facebook.com" target="_blank"  title="FACEBOOK">
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_face.png" alt="" />
          </a>
          <a class="active1">VOCÊ ESTÁ EM:</a>
          <a href="<?php echo Util::caminho_projeto(); ?>/">HOME</i></a>
          <a class="active">PARCEIROS</i></a>
        </div>
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
      </div>



    </div>
  </div>


  <!-- ======================================================================= -->
  <!-- NOSSAS EQUIPES DESCRICAO  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">


      <!-- ======================================================================= -->
      <!-- item 01  -->
      <!-- ======================================================================= -->
      <div class="col-xs-3 top50 equipe_dentro">

        <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 271, 374, array("class"=>"", "alt"=>"$dados_dentro[titulo]") ) ?>
        <div class="top15">

          <!-- ======================================================================= -->
          <!-- E-MAIL -->
          <!-- ======================================================================= -->
          <a href="" data-toggle="modal" data-target="#meu_email"  title="EMAIL">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_equipes_dentro01.png" alt="" />
          </a>
          <!-- Modal -->
          <div class="modal fade" id="meu_email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close equipe_dentro" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h1 class="modal-title" id="myModalLabel"><?php Util::imprime($dados_dentro[titulo]); ?></h1>
                  <h2 class="modal-title top10" id="myModalLabel"><?php Util::imprime($dados_dentro[email]);?></h2>

                <?php require_once('./includes/formulario.php'); ?>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

                </div>
              </div>
            </div>
          </div>
          <!-- ======================================================================= -->
          <!-- E-MAIL -->
          <!-- ======================================================================= -->

          <!-- ======================================================================= -->
          <!-- FACE  -->
          <!-- ======================================================================= -->
          <?php if ($dados_dentro[facebook] != "") { ?>
          <a class="left30" href="<?php Util::imprime($dados_dentro[facebook]); ?>" target="_blank" title="FACEBOOK">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_equipes_dentro02.png" alt="facebook" />
          </a>
          <?php } ?>
          <!-- ======================================================================= -->
          <!-- FACE  -->
          <!-- ======================================================================= -->

          <!-- ======================================================================= -->
          <!-- CONTATOS  -->
          <!-- ======================================================================= -->
          <a class="left30" href="" data-toggle="modal" data-target="#meus_contatos" title="MEUS CONTATOS">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_equipes_dentro03.png" alt="" />
          </a>
          <!-- Modal -->
          <div class="modal fade" id="meus_contatos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close equipe_dentro" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h1 class="modal-title" id="myModalLabel"><?php Util::imprime($dados_dentro[titulo]) ?></h1>
                  <h2 class="modal-title top10" id="myModalLabel"><?php Util::imprime($dados_dentro[telefone]) ?></h2>


                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

                </div>
              </div>
            </div>
          </div>
          <!-- ======================================================================= -->
          <!-- CONTATOS  -->
          <!-- ======================================================================= -->


          <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_equipe.png" class="top15" alt="" />
        </div>
      </div>

      <div class="col-xs-8 top50 equipe_dentro">
        <h1><?php Util::imprime($dados_dentro[titulo]) ?></h1>
        <h2 class="top10"><?php Util::imprime($dados_dentro[cargo]) ?></h2>
        <div class="top25">
          <p>
            <?php Util::imprime($dados_dentro[descricao]) ?>
          </p>
        </div>
      </div>


      <!-- ======================================================================= -->
      <!-- item 01  -->
      <!-- ======================================================================= -->

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- NOSSAS EQUIPES DESCRICAO  -->
  <!-- ======================================================================= -->



  <div class="clearfix">  </div>

  <!-- ======================================================================= -->
  <!-- NOSSAS EQUIPES  -->
  <!-- ======================================================================= -->
  <div class="container top60 bottom200">
    <div class="row text-center">
      <div class="col-xs-12 text-center">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/outros_membros_equipe.png" alt="" />
      </div>


      <!-- ======================================================================= -->
      <!-- item 01  -->
      <!-- ======================================================================= -->
      <?php
      $result = $obj_site->select("tb_equipes", "order by rand()");
      if (mysql_num_rows($result) > 0) {
        while ($row = mysql_fetch_array($result)) {
          ?>

          <div class="col-xs-3 top50 equipe_tipos">
            <a href="<?php echo Util::caminho_projeto() ?>/equipe/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 271, 374, array("class"=>"", "alt"=>"$row[titulo]") ) ?>

              <div class="top15"><h1><?php Util::imprime($row[titulo]) ?></h1></div>
            </a>
            <div class="top15"><h5><?php Util::imprime($row[cargo]) ?></h5></div>
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_equipe.png" class="text-center top20" alt="" />

          </div>
          <?php
          if ($i == 3) {
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }
        }
      }
      ?>


      <!-- ======================================================================= -->
      <!-- item 01  -->
      <!-- ======================================================================= -->

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- NOSSAS EQUIPES  -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>

<!-- ======================================================================= -->
<!-- JS  e CSS   -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- JS  e CSS   -->
<!-- ======================================================================= -->

<script>
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 370,
    itemMargin: 40,

  });
});
</script>



<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      data_visita: {
        validators: {
          notEmpty: {
            message: 'Informe uma data.'

          }
        }
      },
      hora_visita: {
        validators: {
          notEmpty: {
            message: 'Informe um horário de visita.'
          }
        }
      },
      endereco: {
        validators: {
          notEmpty: {
            message: 'Informe seu endereço ?'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'por favor insira seu e-mail'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
