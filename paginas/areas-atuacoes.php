<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 11);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 5) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 144px center no-repeat;
}
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
      <div class="col-xs-8 top55 bottom30 text-center titulo_contatos">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_balanca.png" alt="" />
        <h3 class="top15">CONFIRA NOSSA ÁREA DE ATUAÇÃO</h3>
        <i class="fa fa-angle-down" aria-hidden="true"></i>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
    </div>
  </div>




  <div class="container">
    <div class="row">



      <div class="col-xs-12 bottom10">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
        <div class="breadcrumb">
          <a class="face" href="https://www.facebook.com" target="_blank"  data-toggle="tooltip"  title="FACEBOOK">
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_face.png" alt="" />
          </a>
          <a class="active1">VOCÊ ESTÁ EM:</a>
          <a href="<?php echo Util::caminho_projeto(); ?>/" data-toggle="tooltip" title="HOME">HOME</i></a>
          <a class="active">ÁREA DE ATUAÇÃO</a>
        </div>
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
      </div>


      <div class="col-xs-12">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4) ?>
        <div class="top35 bottom30">
          <p>
            <?php Util::imprime($row[descricao]); ?>
          </p>
        </div>
      </div>

  </div>
</div>


<!-- ======================================================================= -->
<!-- AREA ATUACAO    -->
<!-- ======================================================================= -->
<div class="container bottom30">
  <div class="row">



    <?php
    $result = $obj_site->select("tb_tipos_veiculos", "order by rand()");
    if (mysql_num_rows($result) > 0) {
      while($row = mysql_fetch_array($result)){
        //  busca a qtd de produtos cadastrados
        $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


        switch ($i) {
          case 1:
             $cor_class = 'DIREITO TRIBUTÁRIO';
          break;
          case 2:
             $cor_class = 'DIREITO CIVIL';
          break;
          case 3:
             $cor_class = 'DIREITO DO TRABALHO';
          break;
          case 4:
             $cor_class = 'DIREITO TRIBUTÁRIO';
          break;
          case 5:
             $cor_class = 'DIREITO CIVIL';
          break;

          default:
            $cor_class = 'DIREITO DO TRABALHO';
          break;
        }
        $i++;
      ?>
    <!-- ======================================================================= -->
    <!--ITEM 01 REPETI ATE 6 ITENS   -->
    <!-- ======================================================================= -->
    <div class="col-xs-4 div_personalizado top30 <?php echo $cor_class; ?> ">
      <div class="produtos_home pt50 text-center">
        <a href="<?php echo Util::caminho_projeto(); ?>/area-atuacao/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]); ?>">
          <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
        </a>

        <div class="top20">
          <h1><?php Util::imprime($row[titulo]); ?></h1>

        </div>

          <div class="pg10">
            <p><?php Util::imprime($row[descricao],500); ?></p>
          </div>
          <a class="btn btn_saiba_mais_produtos pb25 right10 pull-right" href="<?php echo Util::caminho_projeto() ?>/area-atuacao/<?php Util::imprime($row[url_amigavel]); ?>" data-toggle="tooltip"  title="<?php Util::imprime($row[titulo]); ?>">
            SAIBA MAIS
          </a>


      </div>
    </div>

    <?php
    }
  }
  ?>



  </div>
</div>
<!-- ======================================================================= -->
<!-- AREA ATUACAO    -->
<!-- ======================================================================= -->







    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->



  </body>

  </html>

  <!-- ======================================================================= -->
  <!-- JS  e CSS   -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/js_css.php') ?>
  <!-- ======================================================================= -->
  <!-- JS  e CSS   -->
  <!-- ======================================================================= -->
