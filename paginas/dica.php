<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
   $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/dicas");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 6) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 144px center no-repeat;
}
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
      <div class="col-xs-8 top55 bottom30 text-center titulo_contatos">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_balanca.png" alt="" />
        <h3 class="top15">CONFIRA NOSSAS NOTÍCIAS</h3>
        <i class="fa fa-angle-down" aria-hidden="true"></i>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
    </div>
  </div>




  <div class="container">
    <div class="row">



      <div class="col-xs-12 bottom10">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
        <div class="breadcrumb">
          <a class="face" href="https://www.facebook.com" target="_blank"  data-toggle="tooltip" title="FACEBOOK">
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_face.png" alt="" />
          </a>
          <a class="active1">VOCÊ ESTÁ EM:</a>
          <a href="<?php echo Util::caminho_projeto(); ?>/" data-toggle="tooltip" title="HOME">HOME</i></a>
          <a class="active">NOTÍCIAS</i></a>
        </div>
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
      </div>



    </div>
  </div>




  <div class="container">
    <div class="row">
      <div class="col-xs-8 top80 dicas_dentro">
        <!-- ======================================================================= -->
        <!-- DESCRICAO DICAS    -->
        <!-- ======================================================================= -->

        <div class="top20">
          <h1><?php Util::imprime($dados_dentro[titulo]) ?></h1>
        </div>
        <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 750, 338, array("class"=>"top35 input100", "alt"=>"$dados_dentro[titulo]") ) ?>
        <div class="top25">
        <p><?php Util::imprime($dados_dentro[descricao]) ?></p>
        </div>
        <!-- ======================================================================= -->
        <!-- DESCRICAO DICAS    -->
        <!-- ======================================================================= -->
      </div>

      <div class="col-xs-4">


        <!-- ======================================================================= -->
        <!-- ULTIMAS FACCEBOK   -->
        <!-- ======================================================================= -->
        <div class="top80">
          <h4>ÚLTIMAS DO FACEBOOK</h4>
        </div>

        <!-- ======================================================================= -->
        <!--COMENTARIOS  REALIZADOS    -->
        <!-- ======================================================================= -->
        <div class="comentarios_realizados_dicas">

          <div class="flexslider">
            <ul class="slides">

              <li class="col-xs-4 padding0">
                <!-- ======================================================================= -->
                <!--ITEM 01 REPETI ATE 3 ITENS   -->
                <!-- ======================================================================= -->
                <div class="div_personalizado top10">
                  <div class="thumbnail face_ultimas pg10">

                    <div class="col-xs-2 padding0">
                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_face_noticias.jpg" class="pull-left bottom15" alt="" />
                    </div>

                    <div class="col-xs-10">
                      <h1>Panzeri & Rodrigues</h1>
                      <h3>23 de março</h3>
                    </div>

                    <img src="<?php echo Util::caminho_projeto() ?>/imgs/ultima_face01.jpg" alt="">
                    <div class="caption top15">
                      <p>Lorem Ipsum is simply dummy text of the printing an
                        d typesetting industry. Lorem Ipsum has been the in
                        dustry's standard dummy text ever since the
                        Lorem Ipsum is simply dummy text of the printing an
                        d typesetting industry. Lorem Ipsum has been the in
                        dustry's standard dummy text ever since the
                      </p>
                      <!-- ======================================================================= -->
                      <!--icons curti ,compartilhar e comentar   -->
                      <!-- ======================================================================= -->
                      <div class="icons_curtir bottom25 top25 pt15">
                        <div class="pull-left left10">
                          <a href="<?php echo Util::caminho_projeto() ?>/">
                            <h2><i class="fa fa-thumbs-up right5"></i>Curtir</h2>
                          </a>
                        </div>

                        <div class="pull-left left10">
                          <a href="<?php echo Util::caminho_projeto() ?>/">
                            <h2><i class="fa fa-comments right5"></i>Cometar</h2>
                          </a>
                        </div>

                        <div class="pull-left left10">
                          <a href="<?php echo Util::caminho_projeto() ?>/">
                            <h2><i class="fa fa-share right5"></i>Compartilhar</h2>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </li>

              <li class="col-xs-4 padding0">
                <!-- ======================================================================= -->
                <!--ITEM 01 REPETI ATE 3 ITENS   -->
                <!-- ======================================================================= -->
                <div class="div_personalizado top10">
                  <div class="thumbnail face_ultimas pg10">

                    <div class="col-xs-2 padding0">
                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_face_noticias.jpg" class="pull-left bottom15" alt="" />
                    </div>

                    <div class="col-xs-10">
                      <h1>Panzeri & Rodrigues</h1>
                      <h3>23 de março</h3>
                    </div>

                    <img src="<?php echo Util::caminho_projeto() ?>/imgs/ultima_face01.jpg" alt="">
                    <div class="caption top15">
                      <p>Lorem Ipsum is simply dummy text of the printing an
                        d typesetting industry. Lorem Ipsum has been the in
                        dustry's standard dummy text ever since the
                        Lorem Ipsum is simply dummy text of the printing an
                        d typesetting industry. Lorem Ipsum has been the in
                        dustry's standard dummy text ever since the
                      </p>
                      <!-- ======================================================================= -->
                      <!--icons curti ,compartilhar e comentar   -->
                      <!-- ======================================================================= -->
                      <div class="icons_curtir bottom25 top25 pt15">
                        <div class="pull-left left10">
                          <a href="<?php echo Util::caminho_projeto() ?>/">
                            <h2><i class="fa fa-thumbs-up right5"></i>Curtir</h2>
                          </a>
                        </div>

                        <div class="pull-left left10">
                          <a href="<?php echo Util::caminho_projeto() ?>/">
                            <h2><i class="fa fa-comments right5"></i>Cometar</h2>
                          </a>
                        </div>

                        <div class="pull-left left10">
                          <a href="<?php echo Util::caminho_projeto() ?>/">
                            <h2><i class="fa fa-share right5"></i>Compartilhar</h2>
                          </a>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

              </li>

              <li class="col-xs-4 padding0">
                <!-- ======================================================================= -->
                <!--ITEM 01 REPETI ATE 3 ITENS   -->
                <!-- ======================================================================= -->
                <div class="div_personalizado top10">
                  <div class="thumbnail face_ultimas pg10">

                    <div class="col-xs-2 padding0">
                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_face_noticias.jpg" class="pull-left bottom15" alt="" />
                    </div>

                    <div class="col-xs-10">
                      <h1>Panzeri & Rodrigues</h1>
                      <h3>23 de março</h3>
                    </div>

                    <img src="<?php echo Util::caminho_projeto() ?>/imgs/ultima_face01.jpg" alt="">
                    <div class="caption top15">
                      <p>Lorem Ipsum is simply dummy text of the printing an
                        d typesetting industry. Lorem Ipsum has been the in
                        dustry's standard dummy text ever since the
                        Lorem Ipsum is simply dummy text of the printing an
                        d typesetting industry. Lorem Ipsum has been the in
                        dustry's standard dummy text ever since the
                      </p>
                      <!-- ======================================================================= -->
                      <!--icons curti ,compartilhar e comentar   -->
                      <!-- ======================================================================= -->
                      <div class="icons_curtir bottom25 top25 pt15">
                        <div class="pull-left left10">
                          <a href="<?php echo Util::caminho_projeto() ?>/">
                            <h2><i class="fa fa-thumbs-up right5"></i>Curtir</h2>
                          </a>
                        </div>

                        <div class="pull-left left10">
                          <a href="<?php echo Util::caminho_projeto() ?>/">
                            <h2><i class="fa fa-comments right5"></i>Cometar</h2>
                          </a>
                        </div>

                        <div class="pull-left left10">
                          <a href="<?php echo Util::caminho_projeto() ?>/">
                            <h2><i class="fa fa-share right5"></i>Compartilhar</h2>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </li>

            </ul>

          </div>
        </div>
        <!-- ======================================================================= -->
        <!--COMENTARIOS  REALIZADOS    -->
        <!-- ======================================================================= -->

       <div class="clearfix"></div>

        <div class="top70">
          <h4>MAIS NOTÍCIAS</h4>
        </div>
        <!-- ======================================================================= -->
        <!--mais dicas repetir ate 4 itens     -->
        <!-- ======================================================================= -->
        <?php
        $result = $obj_site->select("tb_dicas", "order by rand() limit 4");
        if (mysql_num_rows($result) > 0) {
        	while ($row = mysql_fetch_array($result)) {
        	?>

        <div class="col-xs-6 top35 padding0">
          <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 172, 119, array("class"=>"", "alt"=>"$row[titulo]") ) ?>
              </a>
        </div>

        <div class="col-xs-6 top35 padding0">
          <div class="top20 mais_dicas_dentro">
            <p><?php Util::imprime($row[descricao],600) ?></p>
          </div>

        </div>
        <?php
        if ($i == 0) {
          echo '<div class="clearfix"></div>';
          $i = 0;
        }else{
          $i++;
        }
      }
    }
    ?>
        <!-- ======================================================================= -->
        <!--mais dicas repetir ate 4 itens     -->
        <!-- ======================================================================= -->

      </div>

      <!-- ======================================================================= -->
      <!-- ULTIMAS FACEBOOK   -->
      <!-- ======================================================================= -->

    </div>
  </div>




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>

<!-- ======================================================================= -->
<!-- JS  e CSS   -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- JS  e CSS   -->
<!-- ======================================================================= -->


<script>
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 400,
    itemMargin: 0,

  });
});
</script>
