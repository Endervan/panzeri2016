<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 3) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 144px center no-repeat;
}
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
      <div class="col-xs-8 top45 text-center titulo_contatos">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_balanca.png" alt="" />
        <h3 class="top15">SEMPRE QUE PRECISAR FALE CONOSCO</h3>
        <i class="fa fa-angle-down" aria-hidden="true"></i>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
    </div>
  </div>




  <div class="container">
    <div class="row">


      <!-- ======================================================================= -->
      <!--CONTATOS   -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 top10">
        <?php require_once('./includes/contatos.php'); ?>
      </div>
      <!-- ======================================================================= -->
      <!--CONTATOS   -->
      <!-- ======================================================================= -->

      <div class="col-xs-12">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
        <div class="breadcrumb top20">
          <a class="face" href="https://www.facebook.com" target="_blank" data-toggle="tooltip" title="FACEBOOK">
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_face.png" alt="" />
          </a>
          <a class="active1">VOCÊ ESTÁ EM:</a>
          <a href="<?php echo Util::caminho_projeto(); ?>/" data-toggle="tooltip" title="HOME">HOME</i></a>
          <a class="active">FALE CONOSCO</a>
        </div>
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
      </div>


      <!-- ======================================================================= -->
      <!--BARRA PAGINAS E COMO CHEGAR   -->
      <!-- ======================================================================= -->
      <div class="col-xs-8 text-center top30">
        <ul class="barras_links">
          <li class="col-xs-4 active">FALE CONOSCO</li>
          <li class="col-xs-4" >
                <a href="<?php echo Util::caminho_projeto(); ?>/trabalhe-conosco" title="TRABALHE CONOSCO">TRABALHE CONOSCO</a>
            </li>
          <li class="col-xs-4" onclick="$('html,body').animate({scrollTop: $('.map').offset().top}, 2000);" title="COMO CHEGAR">COMO CHEGAR</li>
        </ul>


      </div>
      <!-- ======================================================================= -->
      <!--BARRA PAGINAS E COMO CHEGAR   -->
      <!-- ======================================================================= -->

        <div class="clearfix"></div>

      <div class="col-xs-8 padding0 agende_formulario">

        <div class="top30">



          <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
            <div class="fundo_formulario">
              <!-- formulario orcamento -->
              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback">
                    <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                    <span class="fa fa-home form-control-feedback top20"></span>

                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                    <span class="fa fa-envelope form-control-feedback top20"></span>
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>


              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                    <span class="fa fa-phone form-control-feedback top20"></span>
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="assunto" class="form-control fundo-form1 input-lg input100" placeholder="ASSUNTO">
                    <span class="fa fa-star form-control-feedback top20"></span>
                  </div>
                </div>
              </div>

              <div class="clearfix">  </div>
              <div class="top20">
                <div class="col-xs-12">
                  <div class="form-group input100 has-feedback">
                    <textarea name="mensagem" cols="30" rows="9" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                    <span class="fa fa-pencil form-control-feedback top20"></span>

                  </div>
                </div>
              </div>

              <!-- formulario orcamento -->
              <div class="col-xs-12 text-right">
                <div class="top20 bottom25">
                  <button type="submit" class="btn btn_formulario" name="btn_contato">
                    ENVIAR
                  </button>
                </div>
              </div>

            </div>


            <!--  ==============================================================  -->
            <!-- FORMULARIO-->
            <!--  ==============================================================  -->
          </form>

        </div>
      </div>


    </div>
  </div>

<div class="container-fluid top40 bg_branco_transparente">
  <!-- ======================================================================= -->
  <!-- endereco e mapa    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/endereco_e_mapa.php') ?>
  <!-- ======================================================================= -->
  <!-- endereco e mapa    -->
  <!-- ======================================================================= -->

</div>



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>

<!-- ======================================================================= -->
<!-- JS  e CSS   -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- JS  e CSS   -->
<!-- ======================================================================= -->



          <?php
          //  VERIFICO SE E PARA ENVIAR O EMAIL
          if(isset($_POST[nome]))
          {
            $texto_mensagem = "
            Nome: ".($_POST[nome])." <br />
            Email: ".($_POST[email])." <br />
            Telefone: ".($_POST[telefone])." <br />
            Assunto: ".($_POST[assunto])." <br />


            Mensagem: <br />
            ".(nl2br($_POST[mensagem]))."
            ";


            Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
            Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
            Util::alert_bootstrap("Obrigado por entrar em contato.");
            unset($_POST);
          }


          ?>



<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      data_visita: {
        validators: {
          notEmpty: {
            message: 'Informe uma data.'

          }
        }
      },
      hora_visita: {
        validators: {
          notEmpty: {
            message: 'Informe um horário de visita.'
          }
        }
      },
      endereco: {
        validators: {
          notEmpty: {
            message: 'Informe seu endereço ?'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'por favor insira seu e-mail'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
