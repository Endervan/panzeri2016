<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 13);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 10) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 144px center no-repeat;
}
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
      <div class="col-xs-8 top55 bottom30 text-center titulo_contatos">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_balanca.png" alt="" />
        <h3 class="top15">CONHEÇA NOSSO ESCRITÓRIO</h3>
        <i class="fa fa-angle-down" aria-hidden="true"></i>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
    </div>
  </div>




  <div class="container">
    <div class="row">



      <div class="col-xs-12 bottom10">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
        <div class="breadcrumb">
          <a class="face" href="https://www.facebook.com" target="_blank"  data-toggle="tooltip" title="facebook">
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_face.png" alt="" />
          </a>
          <a class="active1">VOCÊ ESTÁ EM:</a>
          <a href="<?php echo Util::caminho_projeto(); ?>/" data-toggle="tooltip" title="HOME" >HOME</i></a>
          <a class="active">O ESCRITÓRIO</i></a>
        </div>
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
      </div>



    </div>
  </div>




    <div class="container">
      <div class="row top45">

        <!--  ==============================================================  -->
        <!-- CONTATOS -->
        <!--  ==============================================================  -->
        <div class="col-xs-6">
          <?php require_once('./includes/contatos.php'); ?>
        </div>
        <!--  ==============================================================  -->
        <!-- CONTATOS -->
        <!--  ==============================================================  -->

        <!--  ==============================================================  -->
        <!-- AGENDAR VISITA -->
        <!--  ==============================================================  -->
        <div class="col-xs-6 fontCond text-center">

          <a class="btn saiba_como_chegar col-xs-5" onclick="$('html,body').animate({scrollTop: $('.map').offset().top}, 2000);"  title="COMO CHEGAR">
              SAIBA COMO CHEGAR
          </a>

          <a class="btn agendar_vistar_escritorio col-xs-offset-2 col-xs-5" href="<?php echo util::caminho_projeto(); ?>/agende-visita"  title="AGENDER SUA VISITA">
            AGENDAR UMA VISITA
          </a>
        </div>
        <!--  ==============================================================  -->
        <!-- AGENDAR VISITA -->
        <!--  ==============================================================  -->

      </div>
    </div>





    <div class="container">
      <div class="row top30">
        <!-- ======================================================================= -->
        <!-- NOSSA VISÃO    -->
        <!-- ======================================================================= -->
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
        <div class="col-xs-12 empresa_descricao top60">
          <div class="relativo">
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_escritorio02.png" class="img_float" alt="" />
            <a class="btn btn_empresa">
              <h1><?php Util::imprime($row[titulo]); ?></h1>
            </a>
          </div>
          <div class="top20">
            <p>
              <?php Util::imprime($row[descricao]); ?>
            </p>
          </div>
        </div>
        <!-- ======================================================================= -->
        <!-- NOSSA VISÃO    -->
        <!-- ======================================================================= -->

        <div class="clearfix"></div>

        <!-- ======================================================================= -->
        <!-- PERFIL DE TRABALHO    -->
        <!-- ======================================================================= -->
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
        <div class="col-xs-12 empresa_descricao top60">
          <div class="col-xs-offset-6 col-xs-6 padding0 text-right">
            <div class="relativo">
              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_escritorio03.png" class="img_float1" alt="" />
              <a class="btn btn_empresa">
                <h1><?php Util::imprime($row[titulo]); ?></h1>
              </a>
            </div>
          </div>
          <div class="col-xs-12 padding0 top20">
            <p>
              <?php Util::imprime($row[descricao]); ?>
            </p>
          </div>
        </div>
        <!-- ======================================================================= -->
        <!-- PERFIL DE TRABALHO    -->
        <!-- ======================================================================= -->


        <!-- ======================================================================= -->
        <!-- ESTRUTURA ORGANIZACIONAL   -->
        <!-- ======================================================================= -->
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
        <div class="col-xs-12 empresa_descricao top60">
          <div class="relativo">
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_escritorio01.png" class="img_float" alt="" />
            <a class="btn btn_empresa">
              <h1><?php Util::imprime($row[titulo]); ?></h1>
            </a>
          </div>
          <div class="top20">
            <p>
              <?php Util::imprime($row[descricao]); ?>
            </p>
          </div>
        </div>
        <!-- ======================================================================= -->
        <!-- ESTRUTURA ORGANIZACIONAL   -->
        <!-- ======================================================================= -->


      </div>
    </div>


    <div class="container-fluid top80">
      <!-- ======================================================================= -->
      <!-- endereco e mapa    -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/endereco_e_mapa.php') ?>
      <!-- ======================================================================= -->
      <!-- endereco e mapa    -->
      <!-- ======================================================================= -->
    </div>


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>

<!-- ======================================================================= -->
<!-- JS  e CSS   -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- JS  e CSS   -->
<!-- ======================================================================= -->


<script>
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 400,
    itemMargin: 0,

  });
});
</script>
