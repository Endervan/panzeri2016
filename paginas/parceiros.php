<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo",12);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 9) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 144px center no-repeat;
}
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
      <div class="col-xs-8 top55 bottom30 text-center titulo_contatos">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_balanca.png" alt="" />
        <h3 class="top15">PARCEIROS DE NEGÓCIOS</h3>
        <i class="fa fa-angle-down" aria-hidden="true"></i>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
    </div>
  </div>




  <div class="container">
    <div class="row">



      <div class="col-xs-12 bottom10">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
        <div class="breadcrumb">
          <a class="face" href="https://www.facebook.com" target="_blank" data-toggle="tooltip" title="FACEBOOK">
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_face.png" alt="" />
          </a>
          <a class="active1">VOCÊ ESTÁ EM:</a>
          <a href="<?php echo Util::caminho_projeto(); ?>/" data-toggle="tooltip" title="HOME">HOME</i></a>
          <a class="active">PARCEIROS</i></a>
        </div>
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
      </div>




    </div>
  </div>




  <!-- ======================================================================= -->
  <!-- NOSSAS EQUIPES  -->
  <!-- ======================================================================= -->
  <div class="container bottom200">
    <div class="row">

      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
      <div class="col-xs-12 top35">
        <p><?php Util::imprime($row[descricao]) ?></p>
      </div>



        <!--  ==============================================================  -->
        <!-- AGENDAR VISITA -->
        <!--  ==============================================================  -->
        <div class="col-xs-12 fontCond text-right top30">
          <a class="btn agendar_vistar_escritorio text-right" href="<?php echo util::caminho_projeto(); ?>/agende-visita" title="AGENDER SUA VISITA">
            AGENDAR UMA VISITA
          </a>
        </div>
        <!--  ==============================================================  -->
        <!-- AGENDAR VISITA -->
        <!--  ==============================================================  -->




      <!-- ======================================================================= -->
      <!-- item 01  -->
      <!-- ======================================================================= -->
      <?php
      $result = $obj_site->select("tb_equipes", "order by rand()");
      if (mysql_num_rows($result) > 0) {
        while ($row = mysql_fetch_array($result)) {
          ?>

          <div class="col-xs-3 top50 equipe_tipos">
            <a href="<?php echo Util::caminho_projeto() ?>/parceiro/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 271, 374, array("class"=>"", "alt"=>"$row[titulo]") ) ?>

              <div class="top15"><h1><?php Util::imprime($row[titulo]) ?></h1></div>
            </a>
            <div class="top15"><h5><?php Util::imprime($row[cargo]) ?></h5></div>
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_equipe.png" class="text-center top20" alt="" />

          </div>
          <?php
          if ($i == 3) {
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }
        }
      }
      ?>


      <!-- ======================================================================= -->
      <!-- item 01  -->
      <!-- ======================================================================= -->

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- NOSSAS EQUIPES  -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>

<!-- ======================================================================= -->
<!-- JS  e CSS   -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- JS  e CSS   -->
<!-- ======================================================================= -->


<script>
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 370,
    itemMargin: 40,

  });
});
</script>
