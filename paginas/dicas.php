<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 6) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 144px center no-repeat;
}
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
      <div class="col-xs-8 top55 bottom30 text-center titulo_contatos">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_balanca.png" alt="" />
        <h3 class="top15">CONFIRA NOSSAS NOTÍCIAS</h3>
        <i class="fa fa-angle-down" aria-hidden="true"></i>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
    </div>
  </div>




  <div class="container">
    <div class="row">



      <div class="col-xs-12 bottom10">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
        <div class="breadcrumb">
          <a class="face" href="https://www.facebook.com" target="_blank"  data-toggle="tooltip" title="FACEBOOK">
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_face.png" alt="" />
          </a>
          <a class="active1">VOCÊ ESTÁ EM:</a>
          <a href="<?php echo Util::caminho_projeto(); ?>/" data-toggle="tooltip" title="HOME"    >HOME</i></a>
          <a class="active">NOTÍCIAS</i></a>
        </div>
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
      </div>



    </div>
  </div>


  <!-- ======================================================================= -->
  <!-- ULTIMAS FACCEBOK   -->
  <!-- ======================================================================= -->
  <div class="container top60">
    <div class="row">

      <div class="col-xs-12 text-center">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/facebook_utimas.png" alt="" />
      </div>

      <!-- ======================================================================= -->
      <!--COMENTARIOS  REALIZADOS    -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 bottom60 comentarios_realizados">

        <div class="flexslider">
          <ul class="slides">


            <?php

            $rss_tags = array(
                              'title',
                              'link',
                              'guid',
                              'comments',
                              'description',
                              'pubDate',
                              'category',
                              );
            $rss_item_tag = 'item';
            $rss_url = 'https://fbrss.com/feed/b1d37fedb470f3db28b2d92f13d6eb950edc315b_317071915300206.xml';

            $rssfeed = Util::rss_to_array($rss_item_tag, $rss_tags, $rss_url);


            if (count($rssfeed) > 0) {
              foreach ($rssfeed as $key => $value) {


                //  pego a imagem somente
                $str = $value[description];
                preg_match('/(src=["\'](.*?)["\'])/', $str, $match);  //find src="X" or src='X'
                $split = preg_split('/["\']/', $match[0]); // split by quotes
                $imagem = $split[1]; // X between quotes
                $descricao = strip_tags($value[description]);

              ?>
                  <li class="col-xs-4222">
                  <!-- ======================================================================= -->
                  <!--ITEM 01 REPETI 3 ATE 3 ITENS   -->
                  <!-- ======================================================================= -->
                  <div class="div_personalizado top40">
                    <div class="thumbnail face_ultimas pg10">

                      <div class="col-xs-2 padding0">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_face_noticias.jpg" class="pull-left bottom15" alt="" />
                      </div>

                      <div class="col-xs-10">
                        <a href="https://www.facebook.com/panzerirodriguesassociados/" title="" target="_blank">
                          Panzeri & Rodrigues Associados
                        </a>
                        <h3><?php Util::imprime($value[pubDate],10); ?></h3>
                      </div>

                      <?php if (!empty($imagem)) { ?>
                        <a href="<?php echo $value[link] ?>" target="_blank">
                          <img style="width: 347px; height: 131px;" src="<?php echo $imagem ?>" alt="">
                        </a>
                      <?php  } ?>

                      <div class="clearfix">

                      </div>

                      <div class="caption top15">


                        <a href="<?php echo $value[link] ?>" target="_blank">
                          <p><?php echo $descricao ?></p>
                        </a>





                        <!-- ======================================================================= -->
                        <!--icons curti ,compartilhar e comentar   -->
                        <!-- ======================================================================= -->


                        <div class="icons_curtir bottom25 top25 pt15">
                          <div class="pull-left left10">

                            <a href="<?php echo Util::caminho_projeto() ?>/">
                              <h2><i class="fa fa-thumbs-up right5"></i>Curtir</h2>
                            </a>
                          </div>

                          <div class="pull-left left10">
                            <a href="<?php echo Util::caminho_projeto() ?>/">
                              <h2><i class="fa fa-comments right5"></i>Cometar</h2>
                            </a>
                          </div>

                          <div class="pull-left left10">
                            <a href="<?php echo Util::caminho_projeto() ?>/">
                              <h2><i class="fa fa-share right5"></i>Compartilhar</h2>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </li>

              <?php
              }
            }





            ?>




          </ul>

        </div>
      </div>
      <!-- ======================================================================= -->
      <!--COMENTARIOS  REALIZADOS    -->
      <!-- ======================================================================= -->

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- ULTIMAS FACEBOOK   -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- NOSSAS NOTICIAS   -->
  <!-- ======================================================================= -->
  <div class="container top70 bottom100">
    <div class="row">

      <div class="col-xs-12">
        <h3><span>NOSSAS NOTÍCIAS</span></h3>
      </div>

      <?php
      $result = $obj_site->select("tb_dicas","order by rand()");
      if (mysql_num_rows($result) > 0) {
        $i = 0;
        while($row = mysql_fetch_array($result)){
          ?>

          <!-- ======================================================================= -->
          <!-- somente 4 itens home   -->
          <!-- ======================================================================= -->
          <div class="col-xs-3 dicas_home top50">
            <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>"  title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",274, 348, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
              <div class="top10">
                <p>
                  <?php Util::imprime($row[descricao],500); ?>
                </p>
              </div>
                </a>
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_dicas_home.jpg" class="top20 bottom20" alt="" />
            </div>
            <!-- ======================================================================= -->
            <!-- somente 4 itens home   -->
            <!-- ======================================================================= -->
            <?php
            if ($i == 3) {
              echo '<div class="clearfix"></div>';
              $i = 0;
            }else{
              $i++;
            }
          }
        }
        ?>


      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- NOSSAS NOTICIAS   -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>

<!-- ======================================================================= -->
<!-- JS  e CSS   -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- JS  e CSS   -->
<!-- ======================================================================= -->

<script>
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 370,
    itemMargin: 40,

  });
});
</script>
