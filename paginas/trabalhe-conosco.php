<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 4) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 144px center no-repeat;
}
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
      <div class="col-xs-8 top35 text-center titulo_contatos">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_balanca.png" alt="" />
        <h3 class="top15">FAÇA PARTE DA NOSSA ESQUIPE</h3>
        <i class="fa fa-angle-down" aria-hidden="true"></i>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO CONTATOS    -->
      <!-- ======================================================================= -->
    </div>
  </div>




  <div class="container">
    <div class="row">


      <!-- ======================================================================= -->
      <!--CONTATOS   -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 top5">
        <?php require_once('./includes/contatos.php'); ?>
      </div>
      <!-- ======================================================================= -->
      <!--CONTATOS   -->
      <!-- ======================================================================= -->

      <div class="col-xs-12">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
        <div class="breadcrumb top20">
          <a class="face" href="https://www.facebook.com" target="_blank" data-toggle="tooltip" title="FACEBOOK">
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_face.png" alt="" />
          </a>
          <a class="active1">VOCÊ ESTÁ EM:</a>
          <a href="<?php echo Util::caminho_projeto(); ?>/" data-toggle="tooltip" title="HOME">HOME</i></a>
          <a class="active">TRABALHE CONOSCO</a>
        </div>
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
      </div>


      <!-- ======================================================================= -->
      <!--BARRA PAGINAS E COMO CHEGAR   -->
      <!-- ======================================================================= -->
      <div class="col-xs-8 text-center top15">
        <ul class="barras_links">
          <li class="col-xs-4"><a href="<?php echo Util::caminho_projeto(); ?>/fale-conosco" title="FALE CONOSCO">FALE CONOSCO</a></li>
          <li class="col-xs-4 active" >
            <a>TRABALHE CONOSCO</a>
          </li>
          <li class="col-xs-4" onclick="$('html,body').animate({scrollTop: $('.map').offset().top}, 2000);" title="COMO CHEGAR">COMO CHEGAR</li>
        </ul>


      </div>
      <!-- ======================================================================= -->
      <!--BARRA PAGINAS E COMO CHEGAR   -->
      <!-- ======================================================================= -->

      <div class="clearfix"></div>

      <div class="col-xs-8 padding0 agende_formulario">

        <div class="top20">




          <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data">
            <div class="fundo_formulario">
              <!-- formulario orcamento -->
              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                    <span class="fa fa-user form-control-feedback top20"></span>
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="email" class="form-control fundo-form1 input100 input-lg" placeholder="E-MAIL">
                    <span class="fa fa-envelope form-control-feedback top20"></span>
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>


              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="telefone" class="form-control fundo-form1 input100  input-lg" placeholder="TELEFONE">
                    <span class="fa fa-phone form-control-feedback top20"></span>
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="escolaridade" class="form-control fundo-form1 input100  input-lg" placeholder="ESCOLARIDADE">
                    <span class="fa fa-book form-control-feedback top20"></span>
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>




              <div class="top20">

                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="area" class="form-control fundo-form1 input100  input-lg" placeholder="AREA">
                    <span class="fa fa-suitcase form-control-feedback top10"></span>
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback">
                    <input type="text" name="cargo" class="form-control fundo-form1 input100  input-lg" placeholder="CARGO" >
                    <span class="fa fa-briefcase form-control-feedback top10"></span>
                  </div>
                </div>
              </div>


              <div class="clearfix"></div>

              <div class="top20">

                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="cidade" class="form-control fundo-form1 input100  input-lg" placeholder="CIDADE">
                    <span class="fa fa-home form-control-feedback top20"></span>
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="estado" class="form-control fundo-form1 input100  input-lg" placeholder="ESTADO">
                    <span class="fa fa-globe form-control-feedback top20"></span>
                  </div>
                </div>

              </div>


              <div class="clearfix"></div>

              <div class="top20">
                <div class="col-xs-12">
                  <div class="form-group input100 has-feedback ">
                    <input type="file" name="curriculo" class="form-control fundo-form1 input100  input-lg" placeholder="CURRÍCULO">
                    <span class="fa fa-file form-control-feedback top20"></span>
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>


              <div class="top20">
                <div class="col-xs-12">
                  <div class="form-group input100 has-feedback">
                    <textarea name="mensagem" cols="30" rows="7" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                    <span class="fa fa-pencil form-control-feedback top20"></span>
                  </div>
                </div>
              </div>


              <!-- formulario orcamento -->
              <div class="col-xs-12 text-right">
                <div class="top15">
                  <button type="submit" class="btn btn_formulario" name="btn_contato">
                    ENVIAR
                  </button>
                </div>
              </div>

            </div>


          </div>
        </div>


      </div>
    </div>

    <div class="container-fluid top40 bg_branco_transparente">
      <!-- ======================================================================= -->
      <!-- endereco e mapa    -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/endereco_e_mapa.php') ?>
      <!-- ======================================================================= -->
      <!-- endereco e mapa    -->
      <!-- ======================================================================= -->

    </div>

    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->



  </body>

  </html>


  <!-- ======================================================================= -->
  <!-- JS  e CSS   -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/js_css.php') ?>
  <!-- ======================================================================= -->
  <!-- JS  e CSS   -->
  <!-- ======================================================================= -->



  <?php
  //  VERIFICO SE E PARA ENVIAR O EMAIL
  if(isset($_POST[nome]))
  {

    if(!empty($_FILES[curriculo][name])):
      $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
      $texto = "Anexo: ";
      $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
      $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
    endif;

    $texto_mensagem = "
    Nome: ".$_POST[nome]." <br />
    Telefone: ".$_POST[telefone]." <br />
    Email: ".$_POST[email]." <br />
    Escolaridade: ".$_POST[escolaridade]." <br />
    Cargo: ".$_POST[cargo]." <br />
    Área: ".$_POST[area]." <br />
    Cidade: ".$_POST[cidade]." <br />
    Estado: ".$_POST[estado]." <br />
    Mensagem: <br />
    ".nl2br($_POST[mensagem])."

    <br><br>
    $texto
    ";

    Util::envia_email("endvan@gmail.com", utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));

    Util::envia_email($config[email], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
    Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
    Util::alert_bootstrap("Obrigado por entrar em contato.");
    unset($_POST);
  }

  ?>



  <script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        nome: {
          validators: {
            notEmpty: {

            }
          }
        },
        email: {
          validators: {
            notEmpty: {
              message: 'Informe seu E-mail'
            },
            emailAddress: {
              message: 'Esse endereço de email não é válido'
            }
          }
        },
        telefone: {
          validators: {
            notEmpty: {
              message: 'Favor informe seu numero.'
            },
            phone: {
              country: 'BR',
              message: 'Informe um telefone válido.'
            }
          },
        },
        assunto: {
          validators: {
            notEmpty: {

            }
          }
        },
        cidade: {
          validators: {
            notEmpty: {

            }
          }
        },
        estado: {
          validators: {
            notEmpty: {

            }
          }
        },
        escolaridade: {
          validators: {
            notEmpty: {

            }
          }
        },
        curriculo: {
          validators: {
            notEmpty: {
              message: 'Por favor insira seu currículo'
            },
            file: {
              extension: 'doc,docx,pdf,rtf',
              type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
              maxSize: 5*1024*1024,   // 5 MB
              message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
            }
          }
        },
        mensagem: {
          validators: {
            notEmpty: {

            }
          }
        }
      }
    });
  });
  </script>
