
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 12);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 16) ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 281px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->



	<div class="container">
		<div class="row">
			<!-- ======================================================================= -->
			<!-- TITULO CONTATOS    -->
			<!-- ======================================================================= -->
			<div class="col-xs-12 top90 bottom30 text-center titulo_contatos">
				<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_balanca.png" alt="" />
				<h3 class="top15">CONHEÇA NOSSAS EQUIPES</h3>
				<i class="fa fa-angle-down" aria-hidden="true"></i>
			</div>
			<!-- ======================================================================= -->
			<!-- TITULO CONTATOS    -->
			<!-- ======================================================================= -->
		</div>
	</div>






	  <!-- ======================================================================= -->
	  <!-- NOSSAS EQUIPES  -->
	  <!-- ======================================================================= -->
	  <div class="container bottom200">
	    <div class="row text-center">

	      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
	      <div class="col-xs-12 top35 equipes_desc">
	        <p><?php Util::imprime($row[descricao]) ?></p>
	      </div>


	      <!-- ======================================================================= -->
	      <!-- item 01  -->
	      <!-- ======================================================================= -->
	      <?php
	      $result = $obj_site->select("tb_equipes", "order by rand()");
	      if (mysql_num_rows($result) > 0) {
	        while ($row = mysql_fetch_array($result)) {
	          ?>

	          <div class="col-xs-6 top50 equipe_tipos">
	            <a href="<?php echo Util::caminho_projeto() ?>/mobile/equipe/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
	              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 210, 269, array("class"=>"", "alt"=>"$row[titulo]") ) ?>

	              <div class="top10"><h1><?php Util::imprime($row[titulo]) ?></h1></div>
	            </a>
	            <div class="top15"><h5><?php Util::imprime($row[cargo]) ?></h5></div>
	            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/barra_equipe.png" class="text-center top15" alt="" />

	          </div>
	          <?php
	          if ($i == 3) {
	            echo '<div class="clearfix"></div>';
	            $i = 0;
	          }else{
	            $i++;
	          }
	        }
	      }
	      ?>


	      <!-- ======================================================================= -->
	      <!-- item 01  -->
	      <!-- ======================================================================= -->

	    </div>
	  </div>
	  <!-- ======================================================================= -->
	  <!-- NOSSAS EQUIPES  -->
	  <!-- ======================================================================= -->



	<?php require_once('../includes/rodape.php'); ?>

</body>

</html>


<?php require_once('../includes/js_css.php'); ?>


