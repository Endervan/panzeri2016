<div class="col-xs-12 contatos top40">
        <h6><span>ATENDIMENTO</span></h6>
        <!-- contatos -->
        <div class="col-xs-6 top20 padding0">
          <div class="pull-right">
            <a class="btn btn-preto" href="tel:+55<?php Util::imprime($config[telefone1]); ?>" role="button">CHAMAR</a>
          </div>

          <div class="pull-right right10 top5">
            <h4><?php Util::imprime($config[telefone1]); ?></h4>
          </div>
        </div>

        <?php if (!empty($config[telefone2])): ?>
          <div class="col-xs-6 top20 padding0">
            <div class="pull-right">
              <a class="btn btn-preto" href="tel:+55<?php Util::imprime($config[telefone2]); ?>" role="button">CHAMAR</a>
            </div>

            <div class="pull-right right10 top5">
            <h4><?php Util::imprime($config[telefone2]); ?></h4>
            </div>
          </div>
        <?php endif ?>

        <?php if (!empty($config[telefone3])): ?>
          <div class="col-xs-6 top20 padding0">
            <div class="pull-right">
              <a class="btn btn-preto" href="tel:+55<?php Util::imprime($config[telefone3]); ?>" role="button">CHAMAR</a>
            </div>

            <div class="pull-right right10 top5">
            <h4><?php Util::imprime($config[telefone3]); ?></h4>
            </div>
          </div>
        <?php endif ?>

        <?php if (!empty($config[telefone4])): ?>
          <div class="col-xs-6 top20 padding0">
            <div class="pull-right">
              <a class="btn btn-preto" href="tel:+55<?php Util::imprime($config[telefone4]); ?>" role="button">CHAMAR</a>
            </div>

            <div class="pull-right right10 top5">
            <h4><?php Util::imprime($config[telefone4]); ?></h4>
            </div>
          </div>
        <?php endif ?>
        <div class="clearfix"></div>
        <!-- contatos -->
      </div>