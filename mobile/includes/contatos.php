<!--  ==============================================================  -->
<!-- CONTATOS -->
<!--  ==============================================================  -->
<div class="pt10 pb10">
  <div class="col-xs-9 contatos_tel">
    <div class="media pull-right">
      <div class="media-left media-middle ">
        <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_atendimento1.png" alt="">
      </div>
      <div class="media-body latobold">
        <h2 class="media-heading lato-bold">
          <span><?php Util::imprime($config[ddd1]) ?></span><?php Util::imprime($config[telefone1]) ?>
        </h2>



      </div>
    </div>
  </div>

  <div class="col-xs-3 padding0">
    <a class="" href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
      <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_telefones_topo1.png" alt="" />
    </a>
  </div>
</div>
<!--  ==============================================================  -->
<!-- CONTATOS -->
<!--  ==============================================================  -->
