<div class="col-xs-12 padding0 agende_formulario">

  <div class="top30">

    <?php
    //  VERIFICO SE E PARA ENVIAR O EMAIL
    if(isset($_POST[nome]))
    {
      $texto_mensagem = "
      Nome: ".($_POST[nome])." <br />
      Email: ".($_POST[email])." <br />
      Telefone: ".($_POST[telefone])." <br />
      Assunto: ".($_POST[assunto])." <br />


      Mensagem: <br />
      ".(nl2br($_POST[mensagem]))."
      ";

      Util::envia_email('endvan@gmail.com', utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);

      Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
      Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
      Util::alert_bootstrap("Obrigado por entrar em contato.");
      unset($_POST);
    }


    ?>



    <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
      <div class="fundo_formulario">
        <!-- formulario orcamento -->
        <div class="top20">
          <div class="col-xs-6">
            <div class="form-group input100 has-feedback">
              <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
              <span class="fa fa-home form-control-feedback top10"></span>
            </div>
          </div>

          <div class="col-xs-6">
            <div class="form-group  input100 has-feedback">
              <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
              <span class="fa fa-envelope form-control-feedback top10"></span>
            </div>
          </div>
        </div>

        <div class="clearfix"></div>


        <div class="top20">
          <div class="col-xs-6">
            <div class="form-group  input100 has-feedback">
              <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
              <span class="fa fa-phone form-control-feedback top10"></span>
            </div>
          </div>

          <div class="col-xs-6">
            <div class="form-group  input100 has-feedback">
              <input type="text" name="assunto" class="form-control fundo-form1 input-lg input100" placeholder="ASSUNTO">
              <span class="fa fa-star form-control-feedback top10"></span>
            </div>
          </div>
        </div>

        <div class="clearfix">  </div>
        <div class="top20">
          <div class="col-xs-12">
            <div class="form-group input100 has-feedback">
              <textarea name="mensagem" cols="30" rows="9" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
              <span class="fa fa-pencil form-control-feedback top10"></span>

            </div>
          </div>
        </div>

        <!-- formulario orcamento -->
        <div class="col-xs-12 text-right">
          <div class="top20 bottom25">
            <button type="submit" class="btn btn_formulario" name="btn_contato">
              ENVIAR
            </button>
          </div>
        </div>

      </div>


      <!--  ==============================================================  -->
      <!-- FORMULARIO-->
      <!--  ==============================================================  -->
    </form>

  </div>
</div>




<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      data_visita: {
        validators: {
          notEmpty: {
            message: 'Informe uma data.'

          }
        }
      },
      hora_visita: {
        validators: {
          notEmpty: {
            message: 'Informe um horário de visita.'
          }
        }
      },
      endereco: {
        validators: {
          notEmpty: {
            message: 'Informe seu endereço ?'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'por favor insira seu e-mail'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
