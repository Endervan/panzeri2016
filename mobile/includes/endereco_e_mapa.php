
    <div class="row">
      <!--  ==============================================================  -->
      <!--ENDERENCO-->
      <!--  ==============================================================  -->
      <div class="col-xs-12 endereco_geral bottom10 top10">
        <div class="media">
          <div class="media-left">
            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_localizacao.png" alt="">
          </div>
          <div class="media-body">
            <h2 class="media-heading"><?php Util::imprime($config[endereco]); ?></h2>

          </div>
        </div>
      </div>
      <!--  ==============================================================  -->
      <!--ENDERENCO-->
      <!--  ==============================================================  -->

      <!--  ==============================================================  -->
      <!--MAPA-->
      <!--  ==============================================================  -->
      <div class="map">
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="508px" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <!--  ==============================================================  -->
      <!--MAPA-->
      <!--  ==============================================================  -->

    </div>
