
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>
	


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 281px center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->





	  <div class="container">
	    <div class="row">
	      <!-- ======================================================================= -->
	      <!-- TITULO CONTATOS    -->
	      <!-- ======================================================================= -->
	      <div class="col-xs-12 top90 bottom30 text-center titulo_contatos">
	        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_balanca.png" alt="" />
	        <h3 class="top15">CONFIRA NOSSAS NOTÍCIAS</h3>
	        <i class="fa fa-angle-down" aria-hidden="true"></i>
	      </div>
	      <!-- ======================================================================= -->
	      <!-- TITULO CONTATOS    -->
	      <!-- ======================================================================= -->
	    </div>
	  </div>



		<!-- ======================================================================= -->
		<!-- ULTIMAS FACCEBOK   -->
		<!-- ======================================================================= -->
		<div class="container top70">
			<div class="row">

				<div class="col-xs-12">
					<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/facebook_ultimas_grandes.png" alt="" />
				</div>

				<!-- ======================================================================= -->
				<!--COMENTARIOS  REALIZADOS    -->
				<!-- ======================================================================= -->
				<div class="col-xs-12 bottom60 comentarios_realizados">

					<div class="flexslider">
						<ul class="slides">

							<?php
            
            $rss_tags = array(  
                              'title',  
                              'link',  
                              'guid',  
                              'comments',  
                              'description',  
                              'pubDate',  
                              'category',  
                              );  
            $rss_item_tag = 'item';  
            $rss_url = 'https://fbrss.com/feed/b1d37fedb470f3db28b2d92f13d6eb950edc315b_317071915300206.xml';

            $rssfeed = Util::rss_to_array($rss_item_tag, $rss_tags, $rss_url);
 

            if (count($rssfeed) > 0) {
              foreach ($rssfeed as $key => $value) {


                //  pego a imagem somente
                $str = $value[description];
                preg_match('/(src=["\'](.*?)["\'])/', $str, $match);  //find src="X" or src='X'
                $split = preg_split('/["\']/', $match[0]); // split by quotes
                $imagem = $split[1]; // X between quotes
                $descricao = strip_tags($value[description]); 
               
              ?>

                  <li class="col-xs-12 padding0">
                    <div class="top30">
                      <div class="thumbnail face_ultimas pg10">
                        
                        <div class="col-xs-2 padding0">
                          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_face_noticias.jpg" class="pull-left bottom10" alt="" />
                        </div>

                        <div class="col-xs-10">
                          <a href="https://www.facebook.com/panzerirodriguesassociados/" title="" target="_blank">
                            <h1>Panzeri & Rodrigues Associados</h1>
                        </a>
                        <h3><?php Util::imprime($value[pubDate],10); ?></h3>
                        </div>

                        <?php if (!empty($imagem)) { ?>
                          <a href="<?php echo $value[link] ?>" target="_blank">
                            <img style="width: 430px; height: 131px;" src="<?php echo $imagem ?>" alt="">
                          </a>
                        <?php  } ?>  


                        <div class="caption top15">
                          <a href="<?php echo $value[link] ?>" target="_blank">
                            <p><?php echo $descricao ?></p>
                          </a>
                          <!-- ======================================================================= -->
                          <!--icons curti ,compartilhar e comentar   -->
                          <!-- ======================================================================= -->
                          <div class="icons_curtir bottom25 top25 pt15">
                            <div class="pull-left left10">
                              <a href="<?php echo Util::caminho_projeto() ?>/">
                                <h2><i class="fa fa-thumbs-up right5"></i>Curtir</h2>
                              </a>
                            </div>

                            <div class="pull-left left10">
                              <a href="<?php echo Util::caminho_projeto() ?>/">
                                <h2><i class="fa fa-comments right5"></i>Cometar</h2>
                              </a>
                            </div>

                            <div class="pull-left left10">
                              <a href="<?php echo Util::caminho_projeto() ?>/">
                                <h2><i class="fa fa-share right5"></i>Compartilhar</h2>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </li>


              <?php
                }
              }
              ?>


						</ul>

					</div>
				</div>
				<!-- ======================================================================= -->
				<!--COMENTARIOS  REALIZADOS    -->
				<!-- ======================================================================= -->

			</div>
		</div>
		<!-- ======================================================================= -->
		<!-- ULTIMAS FACEBOOK   -->
		<!-- ======================================================================= -->




		  <!-- ======================================================================= -->
		  <!-- NOSSAS DICAS   -->
		  <!-- ======================================================================= -->
		  <div class="container top20 bottom40">
		    <div class="row">

		      <div class="col-xs-12 dicas_home_titulo">
		        <h3><span>NOSSAS NOTÍCIAS</span></h3>
		      </div>


		      <?php
		      $result = $obj_site->select("tb_noticias","order by rand()");
		      if (mysql_num_rows($result) > 0) {
		        $i = 0;
		        while($row = mysql_fetch_array($result)){
		          ?>

		          <!-- ======================================================================= -->
		          <!-- somente 4 itens home   -->
		          <!-- ======================================================================= -->
		          <div class="col-xs-6 dicas_home top30">
		            <a href="<?php echo Util::caminho_projeto() ?>/mobile/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
		              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",210, 267, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
		              <div class="top10">
		                <p>
		                  <?php Util::imprime($row[descricao],500); ?>
		                </p>
		              </div>
		            </a>
		            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/barra_dicas_home.jpg" class="top20 bottom20" alt="" />
		          </div>
		          <!-- ======================================================================= -->
		          <!-- somente 4 itens home   -->
		          <!-- ======================================================================= -->
		          <?php
		          if ($i == 3) {
		            echo '<div class="clearfix"></div>';
		            $i = 0;
		          }else{
		            $i++;
		          }
		        }
		      }
		      ?>


		    </div>
		  </div>
		</div>
		<!-- ======================================================================= -->
		<!-- NOSSAS DICAS   -->
		<!-- ======================================================================= -->










<?php require_once('../includes/rodape.php'); ?>

</body>

</html>

<?php require_once('../includes/js_css.php'); ?>


<script>
	$(window).load(function() {
		$('.flexslider').flexslider({
			animation: "slide",
			animationLoop: true,
			itemWidth: 450,
			itemMargin: 0,

		});
	});
	</script>
