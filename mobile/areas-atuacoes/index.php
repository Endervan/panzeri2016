
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 11);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14) ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 281px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->



	<div class="container">
		<div class="row">
			<!-- ======================================================================= -->
			<!-- TITULO CONTATOS    -->
			<!-- ======================================================================= -->
			<div class="col-xs-12 top90 bottom30 text-center titulo_contatos">
				<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_balanca.png" alt="" />
				<h3 class="top15">CONFIRA NOSSA ÁREA DE ATUAÇÃO</h3>
				<i class="fa fa-angle-down" aria-hidden="true"></i>
			</div>
			<!-- ======================================================================= -->
			<!-- TITULO CONTATOS    -->
			<!-- ======================================================================= -->
		</div>
	</div>






	<div class="container">
		<div class="row">

			<!-- ======================================================================= -->
			<!-- TITULO SOBRE EQUIPE    -->
			<!-- ======================================================================= -->
			<div class="col-xs-12 top30">
				<?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4) ?>
				<div class="top25 bottom20">
					<p>
						<?php Util::imprime($row[descricao]); ?>
					</p>
				</div>
			</div>
			<!-- ======================================================================= -->
			<!-- TITULO SOBRE EQUIPE    -->
			<!-- ======================================================================= -->
		</div>
	</div>


	<!-- ======================================================================= -->
  <!-- AREA ATUACAO    -->
  <!-- ======================================================================= -->
  <div class="container top35">
    <div class="row">

      <?php
      $result = $obj_site->select("tb_tipos_veiculos", "order by rand()");
      if (mysql_num_rows($result) > 0) {
        while($row = mysql_fetch_array($result)){
          //  busca a qtd de produtos cadastrados
          $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


          switch ($i) {
            case 1:
            $cor_class = 'DIREITO TRIBUTÁRIO';
            break;
            case 2:
            $cor_class = 'DIREITO CIVIL';
            break;
            case 3:
            $cor_class = 'DIREITO DO TRABALHO';
            break;
            case 4:
            $cor_class = 'DIREITO TRIBUTÁRIO';
            break;
            case 5:
            $cor_class = 'DIREITO CIVIL';
            break;

            default:
            $cor_class = 'DIREITO DO TRABALHO';
            break;
          }
          $i++;
          ?>
          <!-- ======================================================================= -->
          <!--ITEM 01 REPETI ATE 6 ITENS   -->
          <!-- ======================================================================= -->
          <div class="col-xs-6 top30 bottom25<?php echo $cor_class; ?>">
            <div class="produtos_home pt25 text-center">

              <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" width="48px" height="40px" alt="" />

              <div class="top5">
                <h1><?php Util::imprime($row[titulo]); ?></h1>
              </div>

              <div class="pg10">
                <p><?php Util::imprime($row[descricao],500); ?></p>
              </div>
              <a class="btn btn_saiba_mais_produtos  right10 pull-right" href="<?php echo Util::caminho_projeto() ?>/mobile/area-atuacao/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                SAIBA MAIS
              </a>


            </div>
          </div>

          <?php
        }
      }
      ?>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- AREA ATUACAO    -->
  <!-- ======================================================================= -->



	<?php require_once('../includes/rodape.php'); ?>

</body>

</html>


<?php require_once('../includes/js_css.php'); ?>
