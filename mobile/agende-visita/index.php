
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 14);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>

	<?php require_once('.././includes/head.php'); ?>


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 18) ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 281px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->





	<div class="container">
		<div class="row">
			<!-- ======================================================================= -->
			<!-- TITULO CONTATOS    -->
			<!-- ======================================================================= -->
			<div class="col-xs-6 col-xs-offset-1 top90 bottom20 text-center titulo_contatos">
				<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_balanca.png" alt="" />
				<h3 class="top15">MARQUE UMA VISITA</h3>
				<i class="fa fa-angle-down" aria-hidden="true"></i>
			</div>
			<!-- ======================================================================= -->
			<!-- TITULO CONTATOS    -->
			<!-- ======================================================================= -->
		</div>
	</div>









	<div class="container">
		<div class="row">


			<!-- ======================================================================= -->
			<!--CONTATOS   -->
			<!-- ======================================================================= -->

			<?php require_once('../includes/contatos.php'); ?>

			<!-- ======================================================================= -->
			<!--CONTATOS   -->
			<!-- ======================================================================= -->




			<div class="col-xs-12 padding0">

				<div class="top15">





					<form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
						<div class="fundo_formulario">
							<!-- formulario orcamento -->

							<div class="col-xs-12 top15">
								<div class="form-group input100 has-feedback">
									<input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
									<span class="fa fa-home form-control-feedback top15"></span>
								</div>
							</div>

							<div class="col-xs-12 top15">
								<div class="form-group  input100 has-feedback">
									<input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
									<span class="fa fa-envelope form-control-feedback top15"></span>
								</div>
							</div>




							<div class="col-xs-12 top15">
								<div class="form-group  input100 has-feedback">
									<input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
									<span class="fa fa-phone form-control-feedback top15"></span>
								</div>
							</div>

							<div class="col-xs-12 top15">
								<div class="form-group  input100 has-feedback">
									<input type="text" name="endereco" class="form-control fundo-form1 input-lg input100" placeholder="ENDEREÇO">
									<span class="fa fa-globe form-control-feedback top15"></span>
								</div>
							</div>

							<div class="col-xs-12 top15">
								<div class="form-group  input100 has-feedback">
									<input type="text" name="data_visita" class="form-control fundo-form1 input-lg input100" placeholder="DATA DA VISITA">
									<span class="fa fa-calendar form-control-feedback top15"></span>
								</div>
							</div>

							<div class="col-xs-12 top15">
								<div class="form-group  input100 has-feedback">
									<input type="text" name="hora_visita" class="form-control fundo-form1 input-lg input100" placeholder="HORA DA VISITA">
									<span class="fa fa-clock-o form-control-feedback top15"></span>
								</div>
							</div>

							<div class="col-xs-12 top15">
								<div class="form-group  input100 has-feedback">
									<input type="text" name="conheceu" class="form-control fundo-form1 input-lg input100" placeholder="COMO NOS CONHECEU">
									<span class="fa fa-folder form-control-feedback top5"></span>
								</div>
							</div>

							<div class="col-xs-12 top15">
								<div class="form-group input100 has-feedback">
									<textarea name="mensagem" cols="30" rows="9" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
									<span class="fa fa-pencil form-control-feedback top15"></span>

								</div>
							</div>


							<!-- formulario orcamento -->
							<div class="col-xs-12 text-right">
								<div class="top20 bottom25">
									<button type="submit" class="btn btn_formulario" name="btn_contato">
										ENVIAR
									</button>
								</div>
							</div>

						</div>


						<!--  ==============================================================  -->
						<!-- FORMULARIO-->
						<!--  ==============================================================  -->
					</form>

				</div>
			</div>


		</div>
	</div>

	<div class="container">
		<!-- ======================================================================= -->
		<!-- endereco e mapa    -->
		<!-- ======================================================================= -->
		<div class="col-xs-12">
			<?php require_once('../includes/endereco_e_mapa.php') ?>
		</div>
		<!-- ======================================================================= -->
		<!-- endereco e mapa    -->
		<!-- ======================================================================= -->
	</div>




	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->



</body>

</html>


<?php require_once('../includes/js_css.php') ?>


<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{
	$texto_mensagem = "
	Nome: ".($_POST[nome])." <br />
	Email: ".($_POST[email])." <br />
	Telefone: ".($_POST[telefone])." <br />
	Endereço: ".($_POST[endereco])." <br />
	Data da Visita: ".($_POST[data_visita])." <br />
	Hora da visita: ".($_POST[hora_visita])." <br />
	Como nos Conheçeu: ".($_POST[conheceu])." <br />


	Mensagem: <br />
	".(nl2br($_POST[mensagem]))."
	";


	Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
	Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
	Util::alert_bootstrap("Obrigado por entrar em contato.");
	unset($_POST);
}


?>




<?php require_once('.././includes/head.php'); ?>
	<script>
	$(window).load(function() {
		$('.flexslider').flexslider({
			animation: "slide",
			animationLoop: true,
			itemWidth: 400,
			itemMargin: 0,

		});
	});
	</script>

<script>
$(document).ready(function() {
	$('.FormContatos').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			nome: {
				validators: {
					notEmpty: {
           message: 'Infome seu nome?'
					}
				}
			},
			data_visita: {
				validators: {
					notEmpty: {
						message: 'Informe uma data.'

					}
				}
			},
			hora_visita: {
				validators: {
					notEmpty: {

					}
				}
			},
			endereco: {
				validators: {
					notEmpty: {
					}
				}
			},
			email: {
				validators: {
					notEmpty: {
						message: 'por favor insira seu e-mail'
					},
					emailAddress: {
					}
				}
			},
			telefone: {
				validators: {
					notEmpty: {
						message: 'Informe seu telefone.'
					},
					phone: {
						country: 'BR',
						message: 'Informe um telefone válido.'
					}
				},
			},
			assunto: {
				validators: {
					notEmpty: {

					}
				}
			}
		}
	});
});
</script>
