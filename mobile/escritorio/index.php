
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 13);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>
	


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 281px center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->





	  <div class="container">
	    <div class="row">
	      <!-- ======================================================================= -->
	      <!-- TITULO CONTATOS    -->
	      <!-- ======================================================================= -->
	      <div class="col-xs-12 top90 bottom30 text-center titulo_contatos">
	        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_balanca.png" alt="" />
	        <h3 class="top15">CONHEÇA NOSSO ESCRITÓRIO</h3>
	        <i class="fa fa-angle-down" aria-hidden="true"></i>
	      </div>
	      <!-- ======================================================================= -->
	      <!-- TITULO CONTATOS    -->
	      <!-- ======================================================================= -->
	    </div>
	  </div>









	    <div class="container">
	      <div class="row top30">
	        <!-- ======================================================================= -->
	        <!-- NOSSA VISÃO    -->
	        <!-- ======================================================================= -->
	        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
	        <div class="col-xs-12 empresa_descricao top60">
	          <div class="relativo">
	            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_escritorio02.png" class="img_float" alt="" />
	            <a class="btn btn_empresa">
	              <h1><?php Util::imprime($row[titulo]); ?></h1>
	            </a>
	          </div>
	          <div class="top20">
	            <p>
	              <?php Util::imprime($row[descricao]); ?>
	            </p>
	          </div>
	        </div>
	        <!-- ======================================================================= -->
	        <!-- NOSSA VISÃO    -->
	        <!-- ======================================================================= -->

	        <div class="clearfix"></div>

	        <!-- ======================================================================= -->
	        <!-- PERFIL DE TRABALHO    -->
	        <!-- ======================================================================= -->
	        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
	        <div class="col-xs-12 empresa_descricao top60">
	          <div class="col-xs-offset-2 col-xs-10 padding0 text-right">
	            <div class="relativo">
	              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_escritorio03.png" class="img_float1" alt="" />
	              <a class="btn btn_empresa">
	                <h1><?php Util::imprime($row[titulo]); ?></h1>
	              </a>
	            </div>
	          </div>
	          <div class="col-xs-12 padding0 top20">
	            <p>
	              <?php Util::imprime($row[descricao]); ?>
	            </p>
	          </div>
	        </div>
	        <!-- ======================================================================= -->
	        <!-- PERFIL DE TRABALHO    -->
	        <!-- ======================================================================= -->


	        <!-- ======================================================================= -->
	        <!-- ESTRUTURA ORGANIZACIONAL   -->
	        <!-- ======================================================================= -->
	          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
	        <div class="col-xs-12 empresa_descricao top60">
	          <div class="relativo">
	            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_escritorio01.png" class="img_float" alt="" />
	            <a class="btn btn_empresa">
	              <h1><?php Util::imprime($row[titulo]); ?></h1>
	            </a>
	          </div>
	          <div class="top20">
	            <p>
	              <?php Util::imprime($row[descricao]); ?>
	            </p>
	          </div>
	        </div>
	        <!-- ======================================================================= -->
	        <!-- ESTRUTURA ORGANIZACIONAL   -->
	        <!-- ======================================================================= -->


	      </div>
	    </div>

	    <div class="container top40">
	      <!-- ======================================================================= -->
	      <!-- endereco e mapa    -->
	      <!-- ======================================================================= -->
	      <div class="col-xs-12">
             <?php require_once('../includes/endereco_e_mapa.php') ?>
				</div>
	      <!-- ======================================================================= -->
	      <!-- endereco e mapa    -->
	      <!-- ======================================================================= -->
	    </div>





<?php require_once('../includes/rodape.php'); ?>

</body>

</html>


<?php require_once('../includes/js_css.php'); ?>


<script>
  $(window).load(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      animationLoop: true,
      itemWidth: 400,
      itemMargin: 0,

    });
  });
  </script>
