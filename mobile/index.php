
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>

 

</head>

<body>


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row carroucel-home">

      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!--  ==============================================================  -->
        <!-- ARRUSTE BOLINHAS E SETA DENTRO GRID PARA QLQ RESOLUCAO-->
        <!--  ==============================================================  -->
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <?php
                $result = $obj_site->select("tb_banners", "and tipo_banner = 2 ");
                if(mysql_num_rows($result) > 0){
                  $i = 0;
                  while ($row = mysql_fetch_array($result)) {
                    $imagens[] = $row;
                    ?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
                    <?php
                    $i++;
                  }
                }
                ?>
              </ol>



            </div>
          </div>
        </div>
        <!--  ==============================================================  -->
        <!-- ARRUSTE BOLINHAS E SETA DENTRO GRID PARA QLQ RESOLUCAO-->
        <!--  ==============================================================  -->


        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">


          <?php
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
              ?>
              <div class="item <?php if($i == 0){ echo "active"; } ?>">

                <?php if (!empty($imagem[url])): ?>
                  <a href="<?php Util::imprime($imagem[url]); ?>" >
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                  </a>
                <?php else: ?>
                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                <?php endif; ?>

              </div>
              <?php
              $i++;
            }
          }
          ?>

        </div>




      </div>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- AREA ATUACAO    -->
  <!-- ======================================================================= -->
  <div class="container top35">
    <div class="row">

      <?php
      $result = $obj_site->select("tb_tipos_veiculos", "order by rand() limit 4");
      if (mysql_num_rows($result) > 0) {
        while($row = mysql_fetch_array($result)){
          //  busca a qtd de produtos cadastrados
          $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


          switch ($i) {
            case 1:
            $cor_class = 'DIREITO TRIBUTÁRIO';
            break;
            case 2:
            $cor_class = 'DIREITO CIVIL';
            break;
            case 3:
            $cor_class = 'DIREITO DO TRABALHO';
            break;
            case 4:
            $cor_class = 'DIREITO TRIBUTÁRIO';
            break;
            case 5:
            $cor_class = 'DIREITO CIVIL';
            break;

            default:
            $cor_class = 'DIREITO DO TRABALHO';
            break;
          }
          $i++;
          ?>
          <!-- ======================================================================= -->
          <!--ITEM 01 REPETI ATE 6 ITENS   -->
          <!-- ======================================================================= -->
          <div class="col-xs-6 top30 bottom25<?php echo $cor_class; ?>">
            <div class="produtos_home pt25 text-center">

              <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" width="48px" height="40px" alt="" />

              <div class="top5">
                <h1><?php Util::imprime($row[titulo]); ?></h1>
              </div>

              <div class="pg10">
                <p><?php Util::imprime($row[descricao],500); ?></p>
              </div>
              <a class="btn btn_saiba_mais_produtos  right10 pull-right" href="<?php echo Util::caminho_projeto() ?>/mobile/area-atuacao/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                SAIBA MAIS
              </a>


            </div>
          </div>

          <?php
        }
      }
      ?>


      <div class="col-xs-12 top25 text-center fontCond">
        <a class="btn ver_todas_areas" href="<?php echo Util::caminho_projeto() ?>/mobile/areas-atuacoes">
          VER TODAS AS ÁREAS DE ATUAÇÃO
        </a>
      </div>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- AREA ATUACAO    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- ULTIMAS FACCEBOK   -->
  <!-- ======================================================================= -->
  <div class="container top35">
    <div class="row">

      <div class="col-xs-12 text-center">
        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/facebook_utimas.png" alt="" />
      </div>

      <!-- ======================================================================= -->
      <!--COMENTARIOS  REALIZADOS    -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 bottom60 comentarios_realizados">

        <div class="flexslider">
          <ul class="slides">

            
              


              <?php
            
            $rss_tags = array(  
                              'title',  
                              'link',  
                              'guid',  
                              'comments',  
                              'description',  
                              'pubDate',  
                              'category',  
                              );  
            $rss_item_tag = 'item';  
            $rss_url = 'https://fbrss.com/feed/b1d37fedb470f3db28b2d92f13d6eb950edc315b_317071915300206.xml';

            $rssfeed = Util::rss_to_array($rss_item_tag, $rss_tags, $rss_url);
 

            if (count($rssfeed) > 0) {
              foreach ($rssfeed as $key => $value) {


                //  pego a imagem somente
                $str = $value[description];
                preg_match('/(src=["\'](.*?)["\'])/', $str, $match);  //find src="X" or src='X'
                $split = preg_split('/["\']/', $match[0]); // split by quotes
                $imagem = $split[1]; // X between quotes
                $descricao = strip_tags($value[description]); 
               
              ?>

                  <li class="col-xs-12 padding0">
                    <div class="top30">
                      <div class="thumbnail face_ultimas pg10">
                        
                        <div class="col-xs-2 padding0">
                          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_face_noticias.jpg" class="pull-left bottom10" alt="" />
                        </div>

                        <div class="col-xs-10">
                          <a href="https://www.facebook.com/panzerirodriguesassociados/" title="" target="_blank">
                            <h1>Panzeri & Rodrigues Associados</h1>
                        </a>
                        <h3><?php Util::imprime($value[pubDate],10); ?></h3>
                        </div>

                        <?php if (!empty($imagem)) { ?>
                          <a href="<?php echo $value[link] ?>" target="_blank">
                            <img style="width: 430px; height: 131px;" src="<?php echo $imagem ?>" alt="">
                          </a>
                        <?php  } ?>  


                        <div class="caption top15">
                          <a href="<?php echo $value[link] ?>" target="_blank">
                            <p><?php echo $descricao ?></p>
                          </a>
                          <!-- ======================================================================= -->
                          <!--icons curti ,compartilhar e comentar   -->
                          <!-- ======================================================================= -->
                          <div class="icons_curtir bottom25 top25 pt15">
                            <div class="pull-left left10">
                              <a href="<?php echo Util::caminho_projeto() ?>/">
                                <h2><i class="fa fa-thumbs-up right5"></i>Curtir</h2>
                              </a>
                            </div>

                            <div class="pull-left left10">
                              <a href="<?php echo Util::caminho_projeto() ?>/">
                                <h2><i class="fa fa-comments right5"></i>Cometar</h2>
                              </a>
                            </div>

                            <div class="pull-left left10">
                              <a href="<?php echo Util::caminho_projeto() ?>/">
                                <h2><i class="fa fa-share right5"></i>Compartilhar</h2>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </li>


              <?php
                }
              }
              ?>

              
            
          </ul>

        </div>
      </div>
      <!-- ======================================================================= -->
      <!--COMENTARIOS  REALIZADOS    -->
      <!-- ======================================================================= -->

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- ULTIMAS FACEBOOK   -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- sobre escritorio  -->
  <!-- ======================================================================= -->
      <div class="container bg_servicos_home">
        <div class="row">
          <div class="col-xs-12 top160 bg_transparente_escuro">

            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
            <div class="top25 bottom30">
              <p>
                <?php Util::imprime($row[descricao],1000); ?>
              </p>
            </div>



          </div>

          <div class="fontCond">

            <div class="col-xs-4 top25">
              <a class="btn btn_saiba_mais_escritorio col-xs-12" href="<?php echo Util::caminho_projeto() ?>/mobile/escritorio">SAIBA MAIS</a>
            </div>
            <div class="col-xs-8 top25">
              <a class="btn btn_saiba_mais_escritorio col-xs-12" href="<?php echo Util::caminho_projeto() ?>/mobile/areas-atuacoes">ÁREAS DE ATUAÇÃO</a>
            </div>

            <div class="col-xs-4 top25">
              <a class="btn btn_saiba_mais_escritorio col-xs-12 right25" href="<?php echo Util::caminho_projeto() ?>/mobile/equipes">NOSSAS EQUIPES</a>
            </div>


          </div>

        </div>
      </div>
  <!-- ======================================================================= -->
  <!-- sobre escritorio  -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- NOSSAS NOTICIAS -->
  <!-- ======================================================================= -->
  <div class="container top20 bottom40">
    <div class="row">
      <div class="col-xs-12 text-center"><img class="clearfix" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/barra_dupla.png" alt="" /></div>
      <div class="col-xs-12 text-center"><div class="btn_nossa_noticias">CONFIRA NOSSAS NOTÍCIAS</div></div>


      <?php
      $result = $obj_site->select("tb_noticias","order by rand() LIMIT 2");
      if (mysql_num_rows($result) > 0) {
        $i = 0;
        while($row = mysql_fetch_array($result)){
          ?>

          <!-- ======================================================================= -->
          <!-- somente 4 itens home   -->
          <!-- ======================================================================= -->
          <div class="col-xs-6 dicas_home top30">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",263, 333, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
              <div class="top10">
                <p>
                  <?php Util::imprime($row[descricao],500); ?>
                </p>
              </div>
            </a>
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/barra_dicas_home.jpg" class="top20 bottom20" alt="" />
          </div>
          <!-- ======================================================================= -->
          <!-- somente 4 itens home   -->
          <!-- ======================================================================= -->
          <?php
          if ($i == 3) {
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }
        }
      }
      ?>


    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- NOSSAS NOTICIAS -->
<!-- ======================================================================= -->






  <!-- ======================================================================= -->
  <!-- RODAPE   -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php'); ?>
  <!-- ======================================================================= -->
  <!-- RODAPE   -->
  <!-- ======================================================================= -->


</body>

</html>


<?php require_once('./includes/js_css.php'); ?>


 <script>
  $(window).load(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      animationLoop: true,
      itemWidth: 450,
      itemMargin: 0,

    });
  });
  </script>

