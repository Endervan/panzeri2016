

<div class="container-fluid topo">
  <div class="row">

    <div class="container">
      <div class="row">
        <!--  ==============================================================  -->
        <!-- LOGO -->
        <!--  ==============================================================  -->
        <div class="col-xs-3 top10">
          <a href="<?php echo Util::caminho_projeto() ?>/" data-toggle="tooltip" data-placement="top" title="HOME">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
          </a>
        </div>

        <!--  ==============================================================  -->
        <!-- LOGO -->
        <!--  ==============================================================  -->

        <div class="col-xs-9 padding0">
          <!--  ==============================================================  -->
          <!-- CONTATOS -->
          <!--  ==============================================================  -->
          <div class="col-xs-6 contatos_topo top5">
            <div class="media pull-right">
              <div class="media-left media-middle ">
                <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_atendimento.png" alt="">
              </div>
              <div class="media-body">
                <h2 class="media-heading lato-bold">
                  <span><?php Util::imprime($config[ddd1]) ?></span><?php Util::imprime($config[telefone1]) ?>
                </h2>
              </div>
            </div>
          </div>


          <div class="col-xs-2 seleciona-uf">
                
                <span>Estou em:</span>

                <select class="form-control text-center " id="appearance-select1" onchange="javascript:location.href = this.value;">

                <?php
                $result = $obj_site->select("tb_unidades","limit 5");
                if (mysql_num_rows($result) > 0) {
                  $i = 0;
                  while($row = mysql_fetch_array($result)){
                    ?>
                    <option value="?unidade=<?php echo $row[url_amigavel] ?>" <?php if($config[idunidade] == $row[idunidade] ){ echo 'selected'; } ?> class="btn btn_outras_unidades" ><?php echo Util::imprime($row[titulo]); ?></option>
                    <?php

                  }
                }
                ?>
              </select>
          </div>  

          <!--  ==============================================================  -->
          <!-- CONTATOS -->
          <!--  ==============================================================  -->

          <!--  ==============================================================  -->
          <!-- AGENDA VISITA -->
          <!--  ==============================================================  -->
          <div class="col-xs-4 agendar top5">
            <a href="<?php echo Util::caminho_projeto() ?>/agende-visita">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/agenda_visita.png" alt="" />
            </a>
          </div>
          <!--  ==============================================================  -->
          <!-- AGENDA VISITA -->
          <!--  ==============================================================  -->

          <!--  ==============================================================  -->
          <!-- MENU-->
          <!--  ==============================================================  -->
          <div class="col-xs-12 fontCond menu">
              <div class="menu_topo">
                  <ul>
                      <li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
                          <a href="<?php echo Util::caminho_projeto() ?>/">HOME</a>
                      </li>

                      <li class="<?php if(Url::getURL( 0 ) == "escritorio"){ echo "active"; } ?>">
                          <a href="<?php echo Util::caminho_projeto() ?>/escritorio">O ESCRITÓRIO</a>
                      </li>

                      <li class="<?php if(Url::getURL( 0 ) == "areas-atuacoes" or Url::getURL( 0 ) == "area-atuacao"){ echo "active"; } ?>">
                          <a href="<?php echo Util::caminho_projeto() ?>/areas-atuacoes">ÁREAS DE ATUAÇÃO</a>
                      </li>



                      <li class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>">
                          <a href="<?php echo Util::caminho_projeto() ?>/dicas">NOTÍCIAS</a>
                      </li>

                      <li class="<?php if(Url::getURL( 0 ) == "parceiros" or Url::getURL( 0 ) == "perceiro"){ echo "active"; } ?>">
                          <a href="<?php echo Util::caminho_projeto() ?>/parceiros">PARCEIROS</a>
                      </li>

                      <li class="<?php if(Url::getURL( 0 ) == "faleConosco"){ echo "active"; } ?>">
                          <a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a>
                      </li>


                  </ul>
              </div>
          </div>
          <!--  ==============================================================  -->
          <!-- MENU-->
          <!--  ==============================================================  -->

        </div>

      </div>
    </div>
  </div>
</div>

 <?php /*

    <!--  ==============================================================  -->
    <!--CARRINHO-->
    <!--  ==============================================================  -->
    <div class="col-xs-3 dropdown carrinho_topo top5 bottom10">
      <button class="btn btn_carrinho input100" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="badge"><?php echo count($_SESSION[solicitacoes_produtos])+count($_SESSION[solicitacoes_servicos]); ?></span>
            </button>


            <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dropdownMenu1">

              <!--  ==============================================================  -->
                  <!--sessao adicionar produtos-->
                  <!--  ==============================================================  -->
                <?php
                if(count($_SESSION[solicitacoes_produtos]) > 0)
                {
                    for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                    {
                        $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                        ?>
                        <div class="lista-itens-carrinho">
                            <div class="col-xs-2">
                                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                            </div>
                            <div class="col-xs-8">
                                <h1><?php Util::imprime($row[titulo]) ?></h1>
                            </div>
                            <div class="col-xs-1">
                                <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                            </div>
                        </div>
                        <?php
                                        }
                                    }
                                    ?>


                                    <!--  ==============================================================  -->
                                        <!--sessao adicionar servicos-->
                                        <!--  ==============================================================  -->
                                    <?php
                                    if(count($_SESSION[solicitacoes_servicos]) > 0)
                                    {
                                        for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                                        {
                                            $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                                            ?>
                                            <div class="lista-itens-carrinho">
                                                <div class="col-xs-2">
                                                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                                                </div>
                                                <div class="col-xs-8">
                                                    <h1><?php Util::imprime($row[titulo]) ?></h1>
                                                </div>
                                                <div class="col-xs-1">
                                                    <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                                                </div>
                                            </div>
                                            <?php
                                                            }
                                                        }
                                                        ?>




                <div class="text-right bottom20">
                    <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn-vermelho" >
                        ENVIAR ORÇAMENTO
                    </a>
                </div>
            </div>
        </div>
        <!--  ==============================================================  -->
        <!--CARRINHO-->
        <!--  ==============================================================  -->


*/ ?>
