<div class="media contatos_geral">
  <div class="media-right pr5">
    <img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_telefones_geral.jpg" alt=""/>
  </div>
  <div class="media-body">

    <h4 class="media-heading">
      <span ><?php Util::imprime($config[ddd1]) ?></span> <?php Util::imprime($config[telefone1]) ?>
      <span class="left15"><?php Util::imprime($config[ddd2]) ?></span> <?php Util::imprime($config[telefone2]) ?>
    </h4>
  </div>

</div>
