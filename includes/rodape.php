<div class="clearfix"></div>
<div class="container-fluid rodape top50">
	<div class="row">

		<a href="#" class="scrollup">scrollup</a>
		<div class="container top40">
			<div class="row">

				<!-- ======================================================================= -->
				<!-- MENU    -->
				<!-- ======================================================================= -->
				<div class="col-xs-12">
					<div class="barra_branca">
						<ul class="menu-rodape">
							<li><a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
								<li><a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/escritorio">O ESCRITÓRIO</a></li>
									<li><a class="<?php if(Url::getURL( 0 ) == "areas-atuacoes" or Url::getURL( 0 ) == "area-atuacao"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/areas-atuacoes">ÁREAS DE ATUAÇÃO</a></li>
										<li><a class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/noticias">NOTÍCIAS</a></li>
											<li><a class="<?php if(Url::getURL( 0 ) == "parceiros" or Url::getURL( 0 ) == "parceiro"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/parceiros">PARCEIROS</a></li>
												<li><a class="<?php if(Url::getURL( 0 ) == "contatos"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a></li>
												</ul>
											</div>
										</div>
										<!-- ======================================================================= -->
										<!-- MENU    -->
										<!-- ======================================================================= -->


										<!-- ======================================================================= -->
										<!-- LOGO    -->
										<!-- ======================================================================= -->
										<div class="col-xs-3 top10 bottom15">
											<a href="#">
												<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="" />
											</a>
										</div>
										<!-- ======================================================================= -->
										<!-- LOGO    -->
										<!-- ======================================================================= -->



										<!-- ======================================================================= -->
										<!-- ENDERECO E TELEFONES    -->
										<!-- ======================================================================= -->
										<div class="col-xs-7 top20 telefone_rodape">
											<?php
											$result = $obj_site->select("tb_unidades","limit 2");
											if (mysql_num_rows($result) > 0) {
												$i = 0;
												while($row = mysql_fetch_array($result)){
													?>
													<p class="bottom15"><i class="glyphicon glyphicon-home right15"></i><?php Util::imprime($row[endereco]); ?></p>

											<p class="bottom15">
												<i class="glyphicon glyphicon-earphone right15"></i>
												<?php Util::imprime($row[ddd1]); ?>
												<?php Util::imprime($row[telefone1]); ?>

												</p>
												<?php

											}
										}
										?>
											</div>
											<!-- ======================================================================= -->
											<!-- ENDERECO E TELEFONES    -->
											<!-- ======================================================================= -->


											<div class="col-xs-2 text-right top25">
												<?php if ($config[google_plus] != "") { ?>
													<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
														<i class="fa fa-google-plus right15"></i>
													</a>
													<?php } ?>

													<a href="http://www.homewebbrasil.com.br" target="_blank">
														<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
													</a>
												</div>

											</div>
										</div>
									</div>
								</div>






								<div class="container-fluid">
									<div class="row rodape-preto">
										<div class="col-xs-12 text-center top15 bottom15">
											<h5>© Copyright PANZERI & RODRIGUES</h5>
										</div>
									</div>
								</div>
