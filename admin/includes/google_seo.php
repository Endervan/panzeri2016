<!-- ======================================================================= -->
<!-- GOOGLE SEO    -->
<!-- ======================================================================= -->
<?php if($_SESSION[login][acesso_tags] == 'SIM'): ?>
    
    <div class="col-xs-12 form-group ">
        <label>Title Google</label>
        <input type="text" name="title_google" value="<?php Util::imprime($dados[title_google]) ?>" class="form-control fundo-form1 input100" >
    </div>

    <div class="col-xs-12 form-group ">
        <label>Description padrão</label>
        <input type="text" name="description_google" value="<?php Util::imprime($dados[description_google]) ?>" class="form-control fundo-form1 input100" >
    </div>

    <div class="col-xs-12 form-group ">
        <label>Keywords padrão</label>
        <textarea name="keywords_google" rows="10" class="form-control fundo-form1 input100"><?php Util::imprime($dados[keywords_google]) ?></textarea>
    </div>

<?php endif; ?>
<!-- ======================================================================= -->
<!-- GOOGLE SEO    -->
<!-- ======================================================================= -->