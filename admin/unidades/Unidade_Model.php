<?php
ob_start();
session_start();

class Unidade_Model extends Dao
{

	private $nome_tabela = "tb_unidades";
	private $chave_tabela = "idunidade";
	public $obj_imagem;


	/*	==================================================================================================================	*/
	#	CONSTRUTOR DA CLASSE
	/*	==================================================================================================================	*/
	public function __construct()
	{
            $this->obj_imagem = new Imagem();
            parent::__construct();
	}


	/*	==================================================================================================================	*/
	/*	ORDENA	*/
	/*	==================================================================================================================	*/
	public function atualizar_ordem($ordem){

            if(count($ordem) > 0):

                foreach($ordem as $key=>$orde):

                    $sql = "UPDATE ". $this->nome_tabela ." SET ordem = '$orde' WHERE ". $this->chave_tabela ." = $key ";
                    parent::executaSQL($sql);

                endforeach;

            endif;

	}


	/*	==================================================================================================================	*/
	/*	FORMULARIO	*/
	/*	==================================================================================================================	*/
	public function formulario($dados)
	{
        $obj_jquery = new Biblioteca_Jquery();
        ?>

        <div class="col-xs-12 form-group ">
            <label>Unidade</label>
            <input type="text" name="titulo" value="<?php Util::imprime($dados[titulo]) ?>" class="form-control fundo-form1 input100" >
        </div>

        <div class="col-xs-12 form-group ">
                <label>Endereço</label>
                <input type="text" name="endereco" value="<?php Util::imprime($dados[endereco]) ?>" class="form-control fundo-form1 input100" >
        </div>


        <div class="col-xs-3 form-group ">
                <label>Telefone 1</label>
                <div class="clearfix"></div>
                <div class="col-xs-3 pg0">
                        <input type="text" name="ddd1" value="<?php Util::imprime($dados[ddd1]) ?>" class="form-control fundo-form1 input100" >
                </div>
                <div class="col-xs-8">
                        <input type="text" name="telefone1" value="<?php Util::imprime($dados[telefone1]) ?>" class="form-control fundo-form1 input100" >
                </div>
        </div>

        <div class="col-xs-3 form-group ">
                <label>Telefone 2</label>
                <div class="clearfix"></div>
                <div class="col-xs-3 pg0">
                        <input type="text" name="ddd2" value="<?php Util::imprime($dados[ddd2]) ?>" class="form-control fundo-form1 input100" >
                </div>
                <div class="col-xs-8">
                        <input type="text" name="telefone2" value="<?php Util::imprime($dados[telefone2]) ?>" class="form-control fundo-form1 input100" >
                </div>
        </div>

        <div class="col-xs-3 form-group ">
                <label>Telefone 3</label>
                <div class="clearfix"></div>
                <div class="col-xs-3 pg0">
                        <input type="text" name="ddd3" value="<?php Util::imprime($dados[ddd3]) ?>" class="form-control fundo-form1 input100" >
                </div>
                <div class="col-xs-8">
                        <input type="text" name="telefone3" value="<?php Util::imprime($dados[telefone3]) ?>" class="form-control fundo-form1 input100" >
                </div>
        </div>

        <div class="col-xs-3 form-group ">
                <label>Telefone 4</label>
                <div class="clearfix"></div>
                <div class="col-xs-3 pg0">
                        <input type="text" name="ddd4" value="<?php Util::imprime($dados[ddd4]) ?>" class="form-control fundo-form1 input100" >
                </div>
                <div class="col-xs-8">
                        <input type="text" name="telefone4" value="<?php Util::imprime($dados[telefone4]) ?>" class="form-control fundo-form1 input100" >
                </div>
        </div>



    	<div class="col-xs-12 form-group ">
        	<label>Src Place</label>
            <input type="text" name="src_place" id="src_place" value="<?php Util::imprime($dados[src_place]) ?>" class="form-control fundo-form1 input100"  />
        </div>









        <!-- ======================================================================= -->
        <!-- GOOGLE SEO    -->
        <!-- ======================================================================= -->
        <?php //require_once("../includes/google_seo.php"); ?>
        <!-- ======================================================================= -->
        <!-- GOOGLE SEO    -->
        <!-- ======================================================================= -->





        <!-- ======================================================================= -->
        <!-- VALIDACAO DO FORMULARIO    -->
        <!-- ======================================================================= -->
        <script>
          $(document).ready(function() {
            $('.FormPrincipal').bootstrapValidator({
              message: 'This value is not valid',
              feedbackIcons: {
								valid: 'fa fa-check',
								invalid: 'fa fa-remove',
								validating: 'fa fa-refresh'
              },
              fields: {


              titulo: {
                validators: {
                  notEmpty: {

                  }
                }
              },

              id_categoriaproduto: {
                validators: {
                  notEmpty: {

                  }
                }
              },

              email: {
                validators: {
                  notEmpty: {

                  },
                  emailAddress: {
                    message: 'Esse endereço de email não é válido'
                  }
                }
              },

              mensagem: {
                validators: {
                  notEmpty: {

                  }
                }
              }
            }
          });
          });
        </script>




            <?php
	}




	/*	==================================================================================================================	*/
	/*	EFETUA O CADASTRO	*/
	/*	==================================================================================================================	*/
	public function cadastra($dados)
	{
            //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
            if($_FILES[imagem][name] != "")
            {
                //	EFETUO O UPLOAD DA IMAGEM
                $dados[imagem] = Util::upload_imagem("../../uploads", $_FILES[imagem], "4194304");

                //  CRIO O CROP
                $this->efetua_crop_imagem($dados[imagem], 366, 336);
            }

            //	CADASTRA O USUARIO
            $id = parent::insert($this->nome_tabela, $dados);

            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "CADASTRO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);


            Util::script_msg("Cadastro efetuado com sucesso.");
            Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/cadastra.php");

	}




	/*	==================================================================================================================	*/
	/*	EFETUA A ALTERACAO	*/
	/*	==================================================================================================================	*/
	public function altera($id, $dados)
	{
            //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
            if($_FILES[imagem][name] != "")
            {
                //	EFETUO O UPLOAD DA IMAGEM
                $dados[imagem] = Util::upload_imagem("../../uploads", $_FILES[imagem], "4194304");


                //  CRIO O CROP
                $this->efetua_crop_imagem($dados[imagem], 366, 336);
            }

            parent::update($this->nome_tabela, $id, $dados);


            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "ALTERAÇÃO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);



            Util::script_msg("Alterado com sucesso.");
            Util::script_location(dirname($_SERVER['SCRIPT_NAME']));

	}




	/*	==================================================================================================================	*/
	/*	ATIVA OU DESATIVA	*/
	/*	==================================================================================================================	*/
	public function ativar_desativar($id, $ativo)
	{
            if($ativo == "SIM")
            {
                $sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'NAO' WHERE " . $this->chave_tabela. " = '$id'";
                parent::executaSQL($sql);

                //	ARMAZENA O LOG
                parent::armazena_log("tb_logs_logins", "DESATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
            }
            else
            {
                $sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'SIM' WHERE " . $this->chave_tabela. " = '$id'";
                parent::executaSQL($sql);

                //	ARMAZENA O LOG
                parent::armazena_log("tb_logs_logins", "ATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
            }
	}




	/*	==================================================================================================================	*/
	/*	EXCLUI	*/
	/*	==================================================================================================================	*/
	public function excluir($id)
	{
            //	BUSCA OS DADOS
            $row = $this->select($id);

            $sql = "DELETE FROM " . $this->nome_tabela. " WHERE " . $this->chave_tabela. " = '$id'";
            parent::executaSQL($sql);

            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "EXCLUSÃO DO LOGIN $id, NOME: $row[nome], Email: $row[email]", $sql, $_SESSION[login][idlogin]);
	}




	/*	==================================================================================================================	*/
	/*	BUSCA OS DADOS	*/
	/*	==================================================================================================================	*/
	public function select($id = "")
	{
            if($id != "")
            {
                    $sql = "
                            SELECT
                                    *
                            FROM
                                    " . $this->nome_tabela. "
                            WHERE
                                    " . $this->chave_tabela. " = '$id'
                            ";
                    return mysql_fetch_array(parent::executaSQL($sql));
            }
            else
            {
                    $sql = "
                            SELECT
                                    *
                            FROM
                                    " . $this->nome_tabela. "
                            ORDER BY
                                    ordem desc
                            ";
                    return parent::executaSQL($sql);
            }

	}


	/*	==================================================================================================================	*/
	/*	EFETUA CROP DA IMAGEM	*/
	/*	==================================================================================================================	*/
	public function efetua_crop_imagem($nome_arquivo, $largura, $altura, $alias = 'tumb_', $tipo = 'crop')
	{
            $obj_imagem = new m2brimagem("../../uploads/$nome_arquivo");
            $obj_imagem->redimensiona($largura, $altura, $tipo);
            $obj_imagem->grava("../../uploads/".$alias.$nome_arquivo);
	}




}
?>
